package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.DefaultListModel;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.table.TableModel;
import javax.swing.JList;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import server.Main;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class Accept_SAs {

	private static JSONObject obj;
	
	private JFrame frame;

	/**
	 * Create the application.
	 * @param string 
	 * @throws ParseException 
	 */
	public Accept_SAs(String string) throws ParseException {
		this.obj = (JSONObject) new JSONParser().parse(string);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnAcceptSa = new JButton("Accept SA");
		btnAcceptSa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String key = (String) obj.get("Hash_key");
				System.out.println("in gui accepting key : "+key);
				Main.setAccept_sa(key);
				Main.setHash_key_table(key);
				frame.dispose();
			}
		});
		btnAcceptSa.setBounds(311, 233, 117, 25);
		frame.getContentPane().add(btnAcceptSa);
		
		JButton btnDenySa = new JButton("Deny SA");
		btnDenySa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Main.setAccept_sa(null);
				frame.dispose();
			}
		});
		btnDenySa.setBounds(182, 233, 117, 25);
		frame.getContentPane().add(btnDenySa);
		
		JLabel lblNewLabel = new JLabel("A new SA requests to register : ");
		lblNewLabel.setBounds(12, 12, 268, 15);
		frame.getContentPane().add(lblNewLabel);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(22, 39, 406, 174);
		frame.getContentPane().add(scrollPane);
		
		DefaultListModel listModel = new DefaultListModel();
		
		listModel.addElement(obj.get("Device_Name"));
		listModel.addElement(obj.get("Interface_IP"));
		listModel.addElement(obj.get("Interface_MacAddr"));
		listModel.addElement(obj.get("getOs_Version"));
		listModel.addElement(obj.get("NMap_Version"));
		
		JList list = new JList(listModel);
		scrollPane.setViewportView(list);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
