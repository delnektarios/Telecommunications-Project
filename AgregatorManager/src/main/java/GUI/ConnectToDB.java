package GUI;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JButton;

import server.Main;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class ConnectToDB {

	private JFrame pass;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 *
	 * @return the username field
	 */
	public JTextField getTextField() {
		return textField;
	}

	/**
	 * 
	 * @return passworwd entry
	 */
	public JPasswordField getPasswordField() {
		return passwordField;
	}

	/**
	 * Create the application.
	 */
	public ConnectToDB() {
		initialize();
	}

	/**
	 * Initialize the contents of the pass.
	 */
	private void initialize() {
		setPass(new JFrame());
		getPass().setBounds(100, 100, 450, 300);
		getPass().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getPass().getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(213, 88, 114, 19);
		getPass().getContentPane().add(textField);
		textField.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(213, 119, 114, 19);
		getPass().getContentPane().add(passwordField);
		
		JLabel lblUsername = new JLabel("UserName :");
		lblUsername.setBounds(93, 90, 95, 15);
		getPass().getContentPane().add(lblUsername);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setBounds(93, 121, 85, 15);
		getPass().getContentPane().add(lblPassword);
		
		JButton btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//code to validate entry
				String username = textField.getText();
				//JOptionPane.showMessageDialog(null, username);
				System.out.println(username);
				String password = passwordField.getText();
				//JOptionPane.showMessageDialog(null, password);
				System.out.println(password);
				
				boolean accept = Main.getDb_users().validatePassword(username, password);
				
				Main.getContinue_exec().add(accept);
				
				pass.dispose();
				
				if(accept){
					JOptionPane.showMessageDialog(null, "Successfuly Connected to Database user : "+username+" ");
				}
				else{
					JOptionPane.showMessageDialog(null, "Failed to connect to database due to invalid\n"
							+"username or password!\n"+"Exiting AM!");
				}
			}
		});
		btnConnect.setBounds(319, 233, 117, 25);
		getPass().getContentPane().add(btnConnect);
	}

	/**
	 * 
	 * @return the connection frame
	 */
	public JFrame getPass() {
		return pass;
	}

	public void setPass(JFrame pass) {
		this.pass = pass;
	}
}
