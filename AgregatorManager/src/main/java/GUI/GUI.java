package GUI;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.json.simple.JSONObject;

import cache.Jobs_Keeper;
import database.DB_Jobs;
import database.DB_Results;
import database.DB_SA_Identify;
import database.Nmap_job;
import database.RegistrationForm;
import server.Main;
import database.SA_Results;


/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class GUI {

	private JFrame frame;
	private Jobs_Keeper keeper = new Jobs_Keeper();
	//private class_to_test_lists_in_GUI[] texts = new class_to_test_lists_in_GUI[10];
	private JList list_of_SAs_to_terminate;

	private JList list_of_SAs_status;
	private JList list_of_SAs_to_send_jobs;
	private JList list_of_periodics;
	private JList list_of_SA_results_tab;
	
	private DB_Jobs db_jobs;
	private DB_SA_Identify db_sa_identify;
	
	private RegistrationForm sa_form;
	private Jobs_Keeper jobs_keeper;
	private SA_Results sa_results;
	private DB_Results db_results;
	
	private static List<Integer> periodic_done = new ArrayList<Integer>();
	
	public static List<Integer> getPeriodic_done() {
		return periodic_done;
	}

	public static void setPeriodic_done(List<Integer> periodic_done) {
		GUI.periodic_done = periodic_done;
	}
	
	/**
	 * initializer for the main graphic user interface
	 */
	public GUI() {
		
		db_jobs = new DB_Jobs(Main.getDb_credentials());
		db_sa_identify = new DB_SA_Identify(Main.getDb_credentials());
		sa_form = new RegistrationForm();
		jobs_keeper = Main.getJobs_keeper();
		sa_results = new SA_Results();
		db_results = new DB_Results(Main.getDb_credentials());

		initialize();
	}
	
	/**
	 * Converter for converting java date to sql date
	 * code from : http://stackoverflow.com/questions/530012/how-to-convert-java-util-date-to-java-sql-date
	 * @param date
	 * @return realtime date in sql format
	 */
	private java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
	    return new java.sql.Date(date.getTime());
	}
	
	//private java.sql.Date convertFromJAVADateToSQLDate(java.util.Date javaDate) {
        //java.sql.Date sqlDate = null;
       // if (javaDate != null) {
            //sqlDate = new Date(javaDate.getTime());
       // }
        //return sqlDate;
    //}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(650, 650, 850, 400);
		getFrame().setResizable(false);
		getFrame().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		getFrame().getContentPane().setLayout(new BorderLayout());
		
		frame.addWindowListener(new java.awt.event.WindowAdapter() {
		    @Override
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        if (JOptionPane.showConfirmDialog(frame, 
		            "Are you sure to exit?", "", 
		            JOptionPane.YES_NO_OPTION,
		            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
		        	
		        	//IN THIS POINT I HAVE TO UPDATA THE JOBS AND RESULTS VALITIDY FOR A NEW EXECUTION
		        	for(int i=0;i<Main.getHash_key_table().size();i++){
		        		db_jobs.updateJobDone(Main.getHash_key_table().get(i));
		        		db_results.updateResults(Main.getHash_key_table().get(i));
		        	}
		        	
		        	db_sa_identify.close_database();
		    		db_jobs.close_database();
		    		db_results.close_database();
		            System.exit(0);
		        }
		    }
		});
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		getFrame().getContentPane().add(tabbedPane, BorderLayout.CENTER);
		
		JPanel terminate_sa = new JPanel();
		tabbedPane.addTab("Terminate SA", null, terminate_sa, null);
		terminate_sa.setLayout(null);
		
		JScrollPane scrollPane_terminate_sa_list = new JScrollPane();
		scrollPane_terminate_sa_list.setBounds(12, 12, 684, 254);
		terminate_sa.add(scrollPane_terminate_sa_list);
		
		final DefaultListModel listModel = new DefaultListModel();
		list_of_SAs_to_terminate = new JList(listModel);
		scrollPane_terminate_sa_list.setViewportView(list_of_SAs_to_terminate);
		
		JButton btnTerminateSa = new JButton("Terminate SA");
		btnTerminateSa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int selected = list_of_SAs_to_terminate.getSelectedIndex();
				System.out.println(selected);
				//String SA_termination = "-1, exit(0), true, -1";
				JSONObject obj = new JSONObject();
				obj.put("id", "-1");
				obj.put("parameters", "exit(0)");
				obj.put("periodic", "true");
				obj.put("time", "-1");
				obj.put("sa_id",Main.getHash_key_table().get(selected));
				System.out.println(Main.getHash_key_table().get(selected));
				//updating keeper
				jobs_keeper.putJob(obj);
				Nmap_job job = new Nmap_job();
				job.setIdNMap(Integer.parseInt("-1"));
				job.setCommand("exit(0)");
				job.setPeriodic(true);
				job.setPeriod(Integer.parseInt("-1"));
				job.setSA_Hashkey(Main.getHash_key_table().get(selected));
				
				java.util.Date date = new java.util.Date();
				java.sql.Date jobtime = convertJavaDateToSqlDate(date);
				job.setJobtime(jobtime);
				//updating DB with this job-termination
				//db_jobs.insertJob(job);
				listModel.remove(selected);
				db_sa_identify.updateTerminated(Main.getHash_key_table().get(selected));
				db_jobs.updateJobDone(Main.getHash_key_table().get(selected));
				db_results.updateResults(Main.getHash_key_table().get(selected));
				Main.setSa_to_remove(Main.getHash_key_table().get(selected));
			}
		});
		btnTerminateSa.setBounds(684, 306, 147, 25);
		terminate_sa.add(btnTerminateSa);
		
		JButton refresh_sas_to_terminate = new JButton("Refresh");
		refresh_sas_to_terminate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//refreshing SAs from database
				listModel.clear();
				for(int i=0;i<Main.getHash_key_table().size();i++){
					System.out.println("----IN GUI------");
					System.out.println(Main.getHash_key_table().get(i));
					System.out.println("----IN GUI OUT------");
					RegistrationForm sa_form = db_sa_identify.getSAform(Main.getHash_key_table().get(i));
					String name = sa_form.getDevice_Name();
					System.out.println((name));
					listModel.addElement(name);
					//System.out.println("in GUI " +sa_form.getDevice_Name());
				}
			}
		});
		refresh_sas_to_terminate.setBounds(714, 9, 117, 25);
		terminate_sa.add(refresh_sas_to_terminate);
		
		JPanel sa_handlers = new JPanel();
		tabbedPane.addTab("SAs status", null, sa_handlers, null);
		sa_handlers.setLayout(null);
		
		final JTextArea txtrStatus_of_SAs = new JTextArea();
		txtrStatus_of_SAs.setText("status");
		txtrStatus_of_SAs.setBounds(379, 14, 452, 280);
		sa_handlers.add(txtrStatus_of_SAs);
		
		JScrollPane scrollPane_status_list = new JScrollPane();
		scrollPane_status_list.setBounds(12, 12, 355, 282);
		sa_handlers.add(scrollPane_status_list);
		
		final DefaultListModel listModel1 = new DefaultListModel();
		list_of_SAs_status = new JList(listModel1);
		scrollPane_status_list.setViewportView(list_of_SAs_status);
		list_of_SAs_status.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		
		list_of_SAs_status.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent arg0) {
				//show text for the current SA chosen
				if(arg0.getValueIsAdjusting() == false){			
					 if (list_of_SAs_status.getSelectedIndex() == -1) {
						 //do nothing
						 txtrStatus_of_SAs.setText(null); //emptying text pane
					 } 
					 else {		
						 int selected = list_of_SAs_status.getSelectedIndex();
						 sa_form  = db_sa_identify.getSAform(Main.getHash_key_table().get(selected));
						//show text for the current SA chosen
							DefaultListModel listModel = new DefaultListModel();
								listModel.addElement(sa_form.getDevice_Name());
								listModel.addElement(sa_form.getNMap_Version());
								listModel.addElement(sa_form.getOs_Version());
								String str;
								
								Long currentTime = System.currentTimeMillis();
								
								//TIME INTERVAL SHOULD BE READ FROM PROPERTY FILE ISTEAD OF fixed VALUE 10*1000
								if(currentTime - Main.getTimeStamps().get(selected) < 10*1000){
									str = "ONLINE";
								}else{
									str = "OFFLINE";
								}
								
						 txtrStatus_of_SAs.setText(listModel.toString()+"\nStatus of SA : "+str);
					}
				}
			}	
		});
		
		
		JButton refresh_sa_status_panel = new JButton("Refresh");
		refresh_sa_status_panel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//refreshing SAs from database
				listModel1.clear();
				txtrStatus_of_SAs.setText(null);
				for(int i=0;i<Main.getHash_key_table().size();i++){
					sa_form = db_sa_identify.getSAform(Main.getHash_key_table().get(i));
					listModel1.addElement(sa_form.getDevice_Name());
				}
			}
		});
		refresh_sa_status_panel.setBounds(714, 306, 117, 25);
		sa_handlers.add(refresh_sa_status_panel);
		
		
		JPanel submit_jobs_to_sa = new JPanel();
		tabbedPane.addTab("Submit Jobs", null, submit_jobs_to_sa, null);
		submit_jobs_to_sa.setLayout(null);
		
		JScrollPane scrollPane_submit_jobs_list = new JScrollPane();
		scrollPane_submit_jobs_list.setBounds(12, 12, 432, 260);
		submit_jobs_to_sa.add(scrollPane_submit_jobs_list);
		
		
		final DefaultListModel listModel2 = new DefaultListModel();
		list_of_SAs_to_send_jobs = new JList(listModel2);
		scrollPane_submit_jobs_list.setViewportView(list_of_SAs_to_send_jobs);
		
		final JTextArea txtrText = new JTextArea();
		txtrText.setText("1,O -oX - localhost,true,2");
		txtrText.setBounds(456, 14, 275, 258);
		submit_jobs_to_sa.add(txtrText);
		
		JButton btnSendJob = new JButton("send job");
		btnSendJob.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//sending job or jobs to choosen SA
				int choice = list_of_SAs_to_send_jobs.getSelectedIndex();
				String jobs = txtrText.getText();
				System.out.println(jobs);
				String[] args_jobs = jobs.split("\n");
				for(int i=0;i<args_jobs.length;i++){
					System.out.println(args_jobs[i]);
					String[] args = args_jobs[i].split(",");
					System.out.println(args[0]+"\n"+args[1]+"\n"+args[2]+"\n"+args[3]+"\n");
					Nmap_job job = new Nmap_job();
					try {
						Main.IncrementCounter();
						job.setIdNMap(Main.getCounter());
					} catch (InterruptedException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					job.setCommand(args[1]);
					if(args[2].equals("true")){
						job.setPeriodic(true);
						periodic_done.add(0);
					}
					else{
						job.setPeriodic(false);
					}
					job.setPeriod(Integer.parseInt(args[3]));
					job.setSA_Hashkey(Main.getHash_key_table().get(choice));
					
					java.util.Date date = new java.util.Date();
					java.sql.Date jobtime = convertJavaDateToSqlDate(date);
					
					job.setJobtime(jobtime);
					db_jobs.insertJob(job);
					JSONObject obj = new JSONObject();
					obj.put("id", Integer.toString(job.getIdNMap()));
					obj.put("parameters", args[1]);
					obj.put("periodic", args[2]);
					obj.put("time", args[3]);
					obj.put("sa_id",Main.getHash_key_table().get(choice));
					jobs_keeper.putJob(obj);
				}
			}
		});
		btnSendJob.setBounds(714, 306, 117, 25);
		submit_jobs_to_sa.add(btnSendJob);
		
		JButton btnRefresh_to_submit_jobs = new JButton("Refresh");
		btnRefresh_to_submit_jobs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtrText.setText("1,O -oX - localhost,true,2");
				//refreshing SAs from database
				listModel2.clear();
				for(int i=0;i<Main.getHash_key_table().size();i++){
					sa_form = db_sa_identify.getSAform(Main.getHash_key_table().get(i));
					listModel2.addElement(sa_form.getDevice_Name());
				}
			}
		});
		btnRefresh_to_submit_jobs.setBounds(585, 306, 117, 25);
		submit_jobs_to_sa.add(btnRefresh_to_submit_jobs);
		
		
		
		JPanel delete_periodic_job = new JPanel();
		tabbedPane.addTab("Manage Periodics", null, delete_periodic_job, null);
		delete_periodic_job.setLayout(null);
		
		JScrollPane scrollPane_periodics_list = new JScrollPane();
		scrollPane_periodics_list.setBounds(12, 12, 585, 282);
		delete_periodic_job.add(scrollPane_periodics_list);
		
		final List<Integer> id_list = new ArrayList<Integer>();
		final List<String> hash_list = new ArrayList<String>();
		
		final DefaultListModel listModel3 = new DefaultListModel();
		list_of_periodics = new JList(listModel3);
		scrollPane_periodics_list.setViewportView(list_of_periodics);
		
		JButton btnTerminate_periodic = new JButton("Terminate");
		btnTerminate_periodic.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TERMINATE SELECTED PERIODIC job
				int selected = list_of_periodics.getSelectedIndex();
				System.out.println("\n\n\n\n\nAFTER 1\n\n\n\n\n");
				//message to send to client "periodic_id, Stop, true, periodic"
				String sa_identify_hashkey = hash_list.get(selected);
				System.out.println("\n\n\n\n\nAFTER 2\n\n\n\n\n");
				//upload the message to database
				Nmap_job job = new Nmap_job();
				System.out.println("\n\n\n\n\nAFTER 3\n\n\n\n\n");
				//GETTING THE ID OF THE PERIODIC JOB
				job.setCommand("Stop");

				job.setPeriodic(true);

				job.setPeriod(Integer.parseInt("0"));
				System.out.println("\n\n\n\n\nAFTER 4\n\n\n\n\n");
				System.out.println("\n\n\n\n\nAFTER 5\n\n\n\n\n");
				job.setSA_Hashkey(hash_list.get(selected));
				java.util.Date date = new java.util.Date();
				java.sql.Date jobtime = convertJavaDateToSqlDate(date);
				job.setJobtime(jobtime);
				//db_jobs.insertJob(job);
				//db_jobs.deleteJob(id_list.get(selected));
				System.out.println("\n\n\n\n\nAFTER 6\n\n\n\n\n");
				JSONObject obj = new JSONObject();
				obj.put("id", Integer.toString(id_list.get(selected)));
				System.out.println("id of periodic to terminate : "+id_list.get(selected));
				obj.put("parameters", "Stop");
				obj.put("periodic", "true");
				obj.put("time", "periodic");
				obj.put("sa_id",hash_list.get(selected));
				//add job to the job keeper 
				jobs_keeper.putJob(obj);
				periodic_done.add(selected, 1);
				hash_list.remove(selected);
				db_jobs.updatePeriodicJobDone(id_list.get(selected));
				listModel3.clear();
			}
		});
		btnTerminate_periodic.setBounds(662, 70, 117, 25);
		delete_periodic_job.add(btnTerminate_periodic);
		
		JButton btnRefresh_periodics = new JButton("Refresh");
		btnRefresh_periodics.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listModel3.clear();
				//refreshing periodic jobs from DB
				List<Nmap_job> periodic_jobs = new ArrayList<Nmap_job>();
				periodic_jobs = db_jobs.findPeriodicJob();
				//GETTING THE ID OF THE PERIODIC JOB FOR EACH PERIODIC JOB
				id_list.clear();
				hash_list.clear();
				if(periodic_done.size() == 0){
					return;
				}
				for(int i=0;i<periodic_jobs.size();i++){
					if(periodic_done.get(i) == 0){
						
						id_list.add(periodic_jobs.get(i).getIdNMap());
						hash_list.add(periodic_jobs.get(i).getSA_Hashkey());
						
						listModel3.addElement("periodic job "+periodic_jobs.get(i).getIdNMap()
								+" running :"+periodic_jobs.get(i).getCommand());						
					}
				}
			}
		});
		btnRefresh_periodics.setBounds(662, 22, 117, 25);
		delete_periodic_job.add(btnRefresh_periodics);
		
		JPanel results_per_sa = new JPanel();
		tabbedPane.addTab("SA results", null, results_per_sa, null);
		results_per_sa.setLayout(null);
		
		JScrollPane scrollPane_SA_results_list = new JScrollPane();
		scrollPane_SA_results_list.setBounds(12, 12, 350, 247);
		results_per_sa.add(scrollPane_SA_results_list);
		
		final DefaultListModel listModel4 = new DefaultListModel();
		list_of_SA_results_tab = new JList(listModel4);
		scrollPane_SA_results_list.setViewportView(list_of_SA_results_tab);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(374, 14, 445, 280);
		results_per_sa.add(scrollPane);
		
		final JTextArea show_sa_results_text_area = new JTextArea();
		scrollPane.setViewportView(show_sa_results_text_area);
		
		list_of_SA_results_tab.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent arg0) {
				//show text for the current SA chosen
				if(arg0.getValueIsAdjusting() == false){			
					 if (list_of_SA_results_tab.getSelectedIndex() == -1) {
						 //do nothing
						 show_sa_results_text_area.setText(null); //emptying text pane
					 } 
					 else {		
						 //show text for the current SA chosen
						 int selected = list_of_SA_results_tab.getSelectedIndex();
						 List<SA_Results> all_per_sa = db_results.findResults(Main.getHash_key_table().get(selected));
						 StringBuilder text = new StringBuilder("");
						 show_sa_results_text_area.setText(null); //emptying text pane
						 for(int i=0;i<all_per_sa.size();i++){
							 text.append("scanner : "+all_per_sa.get(i).getResults()+"\n");
							 text.append("command arguments : "+all_per_sa.get(i).getArgument1()+"\n");
							 text.append("start time : "+all_per_sa.get(i).getArgument2()+"\n");
							 text.append("protocol : "+all_per_sa.get(i).getArgument3()+"\n");
							 text.append("summary : "+all_per_sa.get(i).getArgument4()+"\n");
							 text.append("-------------next scan-------------\n");
						 }
						 show_sa_results_text_area.setText(text.toString());
					}
				}
			}	
		});
		
		JButton btnRefresh_sa_results = new JButton("Refresh");
		btnRefresh_sa_results.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//refreshing from DB 
				listModel4.clear();
				//show_sa_results_text_area = new JTextArea(data);
				for(int i=0;i<Main.getHash_key_table().size();i++){
					sa_form = db_sa_identify.getSAform(Main.getHash_key_table().get(i));
					listModel4.addElement(sa_form.getDevice_Name());
				}
				show_sa_results_text_area.setText(null);
			}
		});
		btnRefresh_sa_results.setBounds(714, 306, 117, 25);
		results_per_sa.add(btnRefresh_sa_results);
		
		JPanel all_sa_results = new JPanel();
		tabbedPane.addTab("All Results", null, all_sa_results, null);
		all_sa_results.setLayout(null);
		
		final JTextArea textArea = new JTextArea();
		textArea.setBounds(21, 6, 642, 329);
		all_sa_results.add(textArea);
		
		JButton btnNewButton = new JButton("Refresh");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				textArea.setText(null);
				for(int j=0;j<Main.getHash_key_table().size();j++){
					 List<SA_Results> all_per_sa = db_results.findResults(Main.getHash_key_table().get(j));
					 StringBuilder text = new StringBuilder("");
					 show_sa_results_text_area.setText(null); //emptying text pane
					 text.append("_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ \n");
					 text.append("Results for SA :"+Main.getHash_key_table().get(j)+"\n");
					 for(int i=0;i<all_per_sa.size();i++){
						 text.append("-----------------------------------\n");
						 text.append("scanner : "+all_per_sa.get(i).getResults()+"\n");
						 text.append("command arguments : "+all_per_sa.get(i).getArgument1()+"\n");
						 text.append("start time : "+all_per_sa.get(i).getArgument2()+"\n");
						 text.append("protocol : "+all_per_sa.get(i).getArgument3()+"\n");
						 text.append("summary : "+all_per_sa.get(i).getArgument4()+"\n");
						 text.append("-------------next scan-------------\n");
					 }
						textArea.setText(text.toString());
				}				
			}
		});
		btnNewButton.setBounds(706, 24, 100, 27);
		all_sa_results.add(btnNewButton);
		
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
