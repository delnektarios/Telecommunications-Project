package GUI;

import java.awt.EventQueue;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JButton;

import org.json.simple.JSONObject;

import database.DBUsers;
import server.Main;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class RegisterNewAndroidClient {

	private JFrame frame;
	private JTextField txtAndroidSpecifications;
	private static JSONObject obj;
	private JTextField textField;


	/**
	 * Create the window.
	 * @param obj 
	 */
	public RegisterNewAndroidClient(JSONObject obj) {
		this.obj = obj;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 450, 250);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 12, 283, 158);
		getFrame().getContentPane().add(scrollPane);
		
		DefaultListModel listModel = new DefaultListModel();
		
		listModel.addElement(obj.get("name"));
		listModel.addElement(obj.get("service"));
		listModel.addElement(obj.get("username"));
		
		JList list = new JList(listModel);
		scrollPane.setViewportView(list);
		
		JButton btnAcceptAndroid = new JButton("Accept ");
		btnAcceptAndroid.setBounds(307, 12, 117, 25);
		btnAcceptAndroid.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String username = (String) obj.get("username");
				String password = (String) obj.get("password");

				DBUsers db_users = new DBUsers(Main.getDb_credentials());
				db_users.insertUser(username, password);
				

				db_users.close_database();
				frame.dispose();
			}
		});
		getFrame().getContentPane().add(btnAcceptAndroid);
		
		JButton btnDeny = new JButton("Deny");
		btnDeny.setBounds(307, 53, 117, 25);
		btnDeny.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		getFrame().getContentPane().add(btnDeny);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
