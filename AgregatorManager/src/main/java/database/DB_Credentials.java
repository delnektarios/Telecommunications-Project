package database;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class DB_Credentials {
	public final String username;
	public final String password;
	public final String url;
	
	/**
	 * Constructing Database Credentials
	 * @param url : to link to database
	 * @param username 
	 * @param password
	 */
	public DB_Credentials( String url, String username, String password) {
		this.username = username;
		this.password = password;
		this.url = url;
	}
}
