package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class DB_Jobs extends Database {
	
	/**
	 * Creating linkage to DB_jobs table of Database
	 * @param cred
	 */
	public DB_Jobs(DB_Credentials cred) {
		super(cred);
	}
	/**
	 * Insertion of a new nmap Job
	 * @param sa_identify_hashkey : foreign key
	 * @param id : id of job
	 * @param parameters : parameters of job
	 * @param period : bool that is true for a periodic job
	 * @param periodtime : time of period
	 * @param date : date the job is inserted
	 * @param jobdone : set to true upon completion of job
	 * @return
	 */
	public int insertJob(String sa_identify_hashkey ,int id ,String parameters ,boolean period 
							,int periodtime ,java.sql.Date date ,int jobdone ){
		
		Statement stmt = null;
		ResultSet results = null;
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int periodtemp;
		if (period)periodtemp=1;
		else periodtemp =0;

		try {
			stmt.executeUpdate("INSERT INTO NMap_Jobs(command,periodic,periodtime,SA_Identify_HashKey,jobtime,jobdone) "
					+ "VALUES('" + parameters + "','" + periodtemp + "','" + periodtime 
					+ "',(SELECT HashKey FROM SA_Identify WHERE HashKey = '"+ sa_identify_hashkey + "') ,'"+ date + "','" + jobdone + "')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}	
	
	/**
	 * Inserting a job to db
	 * @param job : object that holds all info of job
	 * @return 0 on success
	 */
	public int insertJob(Nmap_job job) {
		Statement stmt = null;
		ResultSet results = null;
		
		String sa_identify_hashkey = job.getSA_Hashkey();
		String parameters = job.getCommand();
		boolean period = job.isPeriodic();
		int periodtime = job.getPeriod(); 
		java.sql.Date date = job.getJobtime(); 
		int jobdone = job.getJobDone();
		int IdNmap = job.getIdNMap();
		

		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		int periodtemp;
		if (period)periodtemp=1;
		else periodtemp =0;

		try {
			stmt.executeUpdate("INSERT INTO NMap_Jobs(ID_Nmap_Job,command,periodic,periodtime,SA_Identify_HashKey,jobtime,jobdone) "
					+ "VALUES('"+ IdNmap + "','"  + parameters + "','" + periodtemp + "','" + periodtime 
					+ "',(SELECT HashKey FROM SA_Identify WHERE HashKey = '"+ sa_identify_hashkey + "') ,'"+ date + "','" + jobdone + "')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	/**
	 * Seraching for any jobs
	 * @param hashkey : for a specific SA
	 * @return a list of the found jobs (if any)
	 */
	public List<Nmap_job> findJobs(String hashkey) {

		Statement stmt = null;
		ResultSet results = null;
		String password = null;
		
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			results = stmt
					.executeQuery("SELECT `NMap_Jobs`.`ID_Nmap_Job`, `NMap_Jobs`.`command`,"
							+ "`NMap_Jobs`.`periodic`,`NMap_Jobs`.`periodtime`, "
							+ "`NMap_Jobs`.`SA_Identify_HashKey`,`NMap_Jobs`.`jobtime`,`NMap_Jobs`.`jobdone` "
							+ "FROM `serverdata`.`NMap_Jobs` WHERE SA_Identify_HashKey = "+ "'" +hashkey+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (results == null) {
			return null;
		}
		
		List<Nmap_job> jobList = new ArrayList<Nmap_job>();
		
		try {
			while (results.next()) {
				
				if(results.getInt("jobdone") != 1){
					
					Nmap_job nmj = new Nmap_job();
					
					nmj.setIdNMap(results.getInt("ID_Nmap_Job"));
					nmj.setCommand(results.getString("command"));
					nmj.setSA_Hashkey(results.getString("SA_Identify_HashKey"));
					nmj.setPeriodic(results.getBoolean("periodic"));
					nmj.setPeriod(results.getInt("periodtime"));
					nmj.setJobtime(results.getDate("jobtime"));
					nmj.setJobDone(results.getInt("jobdone"));
					
		
					jobList.add(nmj);
					
				}
			}
			results.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jobList;
	}
	
	/**
	 * Finfing all periodic jobs for a SA
	 * @return a list of all periodic jobs if any
	 */
	public List<Nmap_job> findPeriodicJob(){
		
		Statement stmt = null;
		ResultSet results = null;
		String password = null;
		
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			results = stmt
					.executeQuery("SELECT `NMap_Jobs`.`ID_Nmap_Job`,`NMap_Jobs`.`command`,"
							+ "`NMap_Jobs`.`periodic`,`NMap_Jobs`.`periodtime`, "
							+ "`NMap_Jobs`.`SA_Identify_HashKey`,`NMap_Jobs`.`jobtime`,`NMap_Jobs`.`jobdone` "
							+ "FROM `serverdata`.`NMap_Jobs` WHERE periodic = 1 AND jobdone = 0");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (results == null) {
			return null;
		}
		
		List<Nmap_job> jobList = new ArrayList<Nmap_job>();
		
		try {
			while (results.next()) {
				
				if(results.getInt("jobdone") != 1){
					
					Nmap_job nmj = new Nmap_job();
					
					nmj.setIdNMap(results.getInt("ID_Nmap_Job"));
					nmj.setCommand(results.getString("command"));
					nmj.setSA_Hashkey(results.getString("SA_Identify_HashKey"));
					nmj.setPeriodic(results.getBoolean("periodic"));
					nmj.setPeriod(results.getInt("periodtime"));
					nmj.setJobtime(results.getDate("jobtime"));
					nmj.setJobDone(results.getInt("jobdone"));
		
					jobList.add(nmj);
					
				}
			}
			results.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jobList;
			
	}
	
	/**
	 * 
	 * @param hashkey
	 * @return
	 */
	public boolean updateJobDone(String hashkey){
		Statement stmt = null;
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			stmt.executeUpdate("UPDATE NMap_Jobs SET jobdone = TRUE WHERE SA_Identify_HashKey = "+ "'" +hashkey+"'" );
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}
	
	
	public void updatePeriodicJobDone(int id){
		Statement stmt = null;
		String password = null;
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			stmt.executeUpdate("UPDATE NMap_Jobs SET jobdone = TRUE WHERE ID_Nmap_Job=" + id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Deleting Jobs from DB
	 * @param id : of job to be deleted
	 */
 	public void deleteJob(int id) {
		Statement stmt = null;
		String password = null;

		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			stmt.executeUpdate("DELETE FROM `serverdata`.`NMap_Jobs` WHERE ID_Nmap_Job=" + id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
