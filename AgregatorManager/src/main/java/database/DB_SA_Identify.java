package database;

import java.sql.*;

//import server.*;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class DB_SA_Identify extends Database {
	
	/**
	 * Linkage to table SA)Identify of the db. This table holds the SAs info
	 * @param cred
	 */
	public DB_SA_Identify(DB_Credentials cred) {
		super(cred);
	}

	public DB_SA_Identify(String db_url, String user, String password) {
		super(db_url, user, password);
	}

	/**
	 * Requesting all Info for a SA
	 * @param hashkey : to get the right SA
	 * @return the info in an RegistrationForm object
	 */
	public RegistrationForm getSAform(String hashkey) {
		
		System.out.println("insidethe base "+hashkey);
		Statement stmt = null;
		ResultSet results = null;
		RegistrationForm form = new RegistrationForm();

		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			results = stmt.executeQuery("SELECT  HashKey,DeviceName,Interface_IP,OsVersion,NMapVersion,"
							//+ "Interface_MacAddr,Terminate FROM SA_Identify WHERE HashKey=hashkey");
					+ "Interface_MacAddr,Terminate FROM SA_Identify WHERE HashKey='" + hashkey + "'");
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (results == null) {
			return null;
		}

		try {
			if (results.next()) {
				form.setHash_key(results.getString("HashKey"));
				form.setDevice_Name(results.getString("DeviceName"));
				System.out.println("just now device name "+results.getString("DeviceName"));
				form.setInterface_IP(results.getString("Interface_IP"));
				form.setInterface_MacAddr(results.getString("Interface_MacAddr"));
				form.setOs_Version(results.getString("OsVersion"));
				form.setNMap_Version(results.getString("NMapVersion"));
				form.setTerminated(results.getBoolean("Terminate"));
			}
			results.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return form;

	}
	
	/**
	 * Updating status of SA (setting Terminate to true when SA is terminated)
	 * @param hashkey : TO IDENTIFY THE RIGHT SA
	 * @return true on success
	 */
	public boolean updateTerminated(String hashkey){
		Statement stmt = null;
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			stmt.executeUpdate("UPDATE SA_Identify SET Terminate = TRUE WHERE HashKey = '" + hashkey + "'" );
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		return true;
		
	}

	/**
	 * Inserting a new SA
	 * @param form : holds SA info
	 * @return 0 on success
	 */
	int insertSAform(RegistrationForm form) {
		Statement stmt = null;
		int ter;

		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {
			
			if(form.isTerminated()) ter = 1;
			else ter= 0;
				
			
			stmt.executeUpdate("INSERT INTO SA_Identify(HashKey,DeviceName,Interface_IP,OsVersion,NMapVersion"
					+ ",Interface_MacAddr,Terminate) VALUE('"
					+ form.getHash_key()
					+ "','"
					+ form.getDevice_Name()
					+ "','"
					+ form.getInterface_IP()
					+ "','"
					+ form.getOs_Version()
					+ "','"
					+ form.getNMap_Version()
					+ "','"
					+ form.getInterface_MacAddr() 
					+ "','"
					+ ter + "')");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

}
