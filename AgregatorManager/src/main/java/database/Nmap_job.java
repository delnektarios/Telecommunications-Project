package database;


import java.util.Date;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class Nmap_job {
	
	private int idNMap ;
	private String command;
	private boolean periodic;
	private int period;
	private String SA_Hashkey;
	private java.sql.Date jobtime ;
	private int jobDone;
	
	public Nmap_job(){}
	
	/**
	 * Creating an object to hold all nmap job info
	 * @param idnmap
	 * @param command
	 * @param periodic
	 * @param period
	 * @param sa_hashkey
	 * @param jobtime
	 */
	public Nmap_job(int idnmap, String command, boolean periodic,int period ,String sa_hashkey, java.sql.Date jobtime){
		
		this.idNMap=idnmap;
		this.command=command;
		this.periodic=periodic;
		this.period = period;
		this.SA_Hashkey=sa_hashkey;
		this.jobtime=jobtime;
		this.jobDone=0;
	}
	
	public boolean isPeriodic() {
		return periodic;
	}

	public void setPeriodic(boolean periodic) {
		this.periodic = periodic;
	}

	public int getJobDone() {
		return jobDone;
	}

	public void setJobDone(int jobDone) {
		this.jobDone = jobDone;
	}

	public int getIdNMap() {
		return idNMap;
	}

	public void setIdNMap(int idNMap) {
		this.idNMap = idNMap;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	

	public String getSA_Hashkey() {
		return SA_Hashkey;
	}

	public void setSA_Hashkey(String sA_Hashkey) {
		SA_Hashkey = sA_Hashkey;
	}

	public java.sql.Date getJobtime() {
		return jobtime;
	}

	public void setJobtime(java.sql.Date jobtime) {
		this.jobtime = jobtime;
	}

	public int getPeriod() {
		return period;
	}

	public void setPeriod(int period) {
		this.period = period;
	}
	
	

}
