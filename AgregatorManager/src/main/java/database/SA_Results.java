package database;

public class SA_Results {

	private String HashKey ;
	private String argument1;
	private String argument2;
	private String argument3;
	private String argument4;
	private String results;
	
	/**
	 * Locating all results in an object for better handling
	 * @param results : not used
	 * @param hashKey : id of SA
	 * @param argument1 : holds scanner
	 * @param argument2 : holds command line arguments
	 * @param argument3 : holds start time
	 * @param argument4 : holds nmap version
	 */
	public SA_Results(String results, String hashKey, String argument1,
			String argument2, String argument3, String argument4) {
		super();
		this.results = results;
		this.HashKey = hashKey;
		this.argument1 = argument1;
		this.argument2 = argument2;
		this.argument3 = argument3;
		this.argument4 = argument4;
	}
	
	public SA_Results(){}
	
	
	public String getResults() {
		return results;
	}

	public void setResults(String results) {
		this.results = results;
	}

	public String getHashKey() {
		return HashKey;
	}

	public void setHashKey(String hashKey) {
		HashKey = hashKey;
	}

	public String getArgument1() {
		return argument1;
	}

	public void setArgument1(String argument1) {
		this.argument1 = argument1;
	}

	public String getArgument2() {
		return argument2;
	}

	public void setArgument2(String argument2) {
		this.argument2 = argument2;
	}

	public String getArgument3() {
		return argument3;
	}

	public void setArgument3(String argument3) {
		this.argument3 = argument3;
	}

	public String getArgument4() {
		return argument4;
	}

	public void setArgument4(String argument4) {
		this.argument4 = argument4;
	}

	
}
