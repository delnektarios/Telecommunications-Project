package server;

import java.sql.*;

import server.Database;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class DBUsers extends Database {
		
	/**
	 * Table that holds the users allow to enter the database
	 * @param cred : DBcredentials object
	 */
	public DBUsers(DB_Credentials cred) {
		super(cred);
	}

	String getNamePass(String username){
		Statement stmt = null;
		ResultSet results=null;
		String password=null;
		
		System.out.println("Creating statement...");
	    try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    try {
			results = stmt.executeQuery("SELECT username ,password  FROM users WHERE username='"+username+"'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    if(results==null){ 
	    	return null;
	    }
	    
	    try {
			if(results.next()){
				password = results.getString("password");
			}
			results.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	   return password;
	}
	
	public boolean validatePassword(String username, String password) {
		String p = getNamePass(username);
		if (p == null) {
			return false;
		} else if (p.equals(password)) {
			return true;
		} else {
			return false;
		}
	}

	/**\
	 * iNSERTING A NEW USER
	 * @param username
	 * @param password
	 * @return 0 on success
	 */
	int insertUser(String username,String password){
		Statement stmt = null;
		ResultSet results=null;
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			stmt.executeUpdate("INSERT INTO users(username ,password) VALUE("+"'" +username +"','"+password +"')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 0;
	}

}
