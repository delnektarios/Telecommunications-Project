package server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class DB_Results extends Database  {
	public DB_Results(DB_Credentials cred) {
		super(cred);
	}

	/**
	 * Inserting Results to db
	 * @param res : object that holds the results
	 * @return 0 on success
	 */
	public int insertResults(SA_Results res){
		
		Statement stmt = null;
		ResultSet results = null;
		
		String re = res.getResults();
		String hashKey = res.getHashKey();
		String arg1 = res.getArgument1();
		String arg2 = res.getArgument2();
		String arg3 = res.getArgument3();
		String arg4 = res.getArgument4();

		System.out.println("Creating statement to upload results");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			stmt.executeUpdate("INSERT INTO NMap_results(Results,SA_Identify_HashKey"
								+ ",argument1,argument2,argument3,argument4)"
								+ " VALUES('"+ re + "'," + "(SELECT HashKey "
								+ "FROM SA_Identify WHERE HashKey ='" + hashKey +"')"
								+ ",'" + arg1 + "','" + arg2 + "','" + arg3 + "','" + arg4 + "')");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

		
	/**
	 * Finding results from a single SA
	 * @param hashKey : to identify the desired SA (primary key)
	 * @return the results
	 */
	public List<SA_Results> findResults(String hashKey) {

		Statement stmt = null;
		ResultSet results = null;
		String password = null;
		
		
		List<SA_Results> resultlist = new ArrayList<SA_Results>();

		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			results = stmt
					.executeQuery("SELECT `NMap_results`.`SA_Identify_HashKey`,`NMap_results`.`results`, `NMap_results`.`argument1`"
							+ ",`NMap_results`.`argument2`,`NMap_results`.`argument3`,`NMap_results`.`argument4`"
							+" FROM `serverdata`.`NMap_results` WHERE SA_Identify_HashKey = '" + hashKey + "'");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (results == null) {
			return null;
		}

		
		try {
			while (results.next()) {
				
				SA_Results saRes = new SA_Results();
				
				saRes.setResults(results.getString("Results"));
				saRes.setHashKey(results.getString("SA_Identify_HashKey"));
				
				saRes.setArgument1(results.getString("argument1"));
				saRes.setArgument2(results.getString("argument2"));
				saRes.setArgument3(results.getString("argument3"));
				saRes.setArgument4(results.getString("argument4"));
				
				resultlist.add(saRes);

			}
			results.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return resultlist;
	}

	
	
}
