package server;

import java.io.IOException;
import java.sql.*;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class Database {
	final String DB_URL;
	final String USER;
	final String PASSWORD;
	Connection conn ;

	/**
	 * Constructor 1
	 * @param cred : holds database url,username and pasword for validation
	 */
	public Database(DB_Credentials cred) {
		this(cred.url, cred.username, cred.password);
	}
	/**
	 * Constructor 2
	 * @param db_url : link to database
	 * @param user : username of user that connected to database
	 * @param password :user's password
	 */
	public Database(String db_url,String user,String password){

		DB_URL=db_url;
		USER=user;
		PASSWORD=password;
		
		System.out.printf("Connecting to database %s ",DB_URL);
		try {
			conn= DriverManager.getConnection(DB_URL, USER, PASSWORD);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		System.out.printf("Connected to database %s ",DB_URL);
	}
	
	/**
	 * Importing Database
	 * @param path : to database file
	 * @param url : to connect to db
	 * @param user : username for user to enter db
	 * @param password : user's password
	 * @param dbname : database name
	 */
	public static void importDB(String path ,String url, String user ,String password, String dbname){
		
		//String command="mysql -u " + user+ "-p " + dbname + " < " + path;
		String command = path; // path to script.
		ProcessBuilder builder = new ProcessBuilder(command);
		
		
		Connection cone = null;
		Statement stmt=null;
		try {
			cone = DriverManager.getConnection(url,user, password);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		try {
			stmt = cone.createStatement();
		} catch (SQLException e) {
			 //TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(-1);
		}
		
		try {
			stmt.executeUpdate("CREATE DATABASE "+ dbname);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			final Process process = builder.start();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			cone.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
 	boolean create_database(String name){
		Statement stmt = null;
		
		System.out.printf("Creating database %s",name);
		
		try {
			stmt  = conn.createStatement();
			stmt.executeUpdate("CREATE DATABASE"+name);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	/**
	 * Creating Table
	 * @param tableName : name of the table to be created
	 * @return number of columns
	 */
	int create_table(String tableName){
		
		Statement stmt = null;
		System.out.printf("Creating table %s",tableName);
		
		try {
			stmt  = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			stmt.executeUpdate("CREATE TABLE REGISTRATION " +
	                "(id INTEGER not NULL, " +
	                " first VARCHAR(255), " + 
	                " last VARCHAR(255), " + 
	                " age INTEGER, " + 
	                " PRIMARY KEY ( id ))");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return 5;
		
		
		
	}
	
	/**
	 * Closing an open database
	 * @return 0 on success
	 */
	int close_database(){
		try {
			conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	
	int drop_database(String name){
		Statement stmt = null;
		
		System.out.println("Creating statement...");
		try {
			stmt = conn.createStatement();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			stmt.executeUpdate("DROP DATABASE "+name);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
}
	
	


