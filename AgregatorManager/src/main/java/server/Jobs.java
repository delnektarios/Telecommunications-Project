package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
@Path("/Jobs")
public class Jobs {
	    
    // The Java method will process HTTP GET requests
    @GET 
    @Path("/getJob/{hashkey}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getIt(@PathParam("hashkey") String hashkey) {
    	System.out.println("received id : "+hashkey);
    	
    	int position = 0;
    	
    	Long currentTime = System.currentTimeMillis();
    	
    	ArrayList<Long> timeStamp = Main.getTimeStamps();
    	
    	for(int i=0; i<Main.getHash_key_table().size();i++){
    		if(Main.getHash_key_table().get(i).equals(hashkey)){
    			position = i;
    		}
    	}
    	
    	timeStamp = Main.getTimeStamps();
    	timeStamp.add(position, currentTime);   	  	
    	Main.setTimeStamps(timeStamp);
    	
    	List<JSONObject> jobs = Main.getJobs_keeper().getJobs(hashkey);
  	  	System.out.print(jobs.toString());
  	  	if(hashkey.equals(Main.getSa_to_remove())){
  	  		Main.remove_sa_key(Main.getSa_to_remove());
  	  	}
  	  	//return Response.status(200).entity(obj.toJSONString()).build();
  	  	return jobs.toString();
    }
    
	 @PUT
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Path("/requestJob")
	 public void InformServerForSA(InputStream xml) throws ParseException {
		 String line = "";
		 StringBuilder sb = new  StringBuilder();
	  	 try {
	       	BufferedReader br = new BufferedReader(new InputStreamReader(xml));
	       	while ((line = br.readLine()) != null) {
	           	sb.append(line);
	       	}
	       	} catch (IOException e) {
	          	e.printStackTrace();
	       	}
	  	JSONObject obj = (JSONObject) new JSONParser().parse(sb.toString());
	  	Main.setSa_to_send_job((String) obj.get("SA_ID"));	  	
	}
	
	 @PUT
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Path("/results")
	 public void putIntoDAO(InputStream xml) throws ParseException {
		 String line = "";
		 StringBuilder sb = new  StringBuilder();
	  	 try {
	       	BufferedReader br = new BufferedReader(new InputStreamReader(xml));
	       	while ((line = br.readLine()) != null) {
	           	sb.append(line);
	       	}
	       	} catch (IOException e) {
	          	e.printStackTrace();
	       	}
	  		//obj.put("mesage",sb.toString());
	  	 
	  	JSONObject obj = (JSONObject) new JSONParser().parse(sb.toString());
	  	
	  	SA_Results res = new SA_Results(obj.get("scanner").toString(),obj.get("Hash_key").toString(),obj.get("cmd_arguments").toString(),
	  			obj.get("start").toString(),obj.get("startstr").toString(),obj.get("version").toString());
	  	
	  	DB_Results db_res = new DB_Results(Main.getDb_credentials());
	  	db_res.insertResults(res);
	  	db_res.close_database();
	  	
	  	String hashkey = (String) obj.get("Hash_key");
	  	
    	int position = 0;
    	
    	Long currentTime = System.currentTimeMillis();
    	
    	ArrayList<Long> timeStamp = Main.getTimeStamps();
    	
    	for(int i=0; i<Main.getHash_key_table().size();i++){
    		if(Main.getHash_key_table().get(i).equals(hashkey)){
    			position = i;
    		}
    	}
    	
    	timeStamp = Main.getTimeStamps();
    	timeStamp.add(position, currentTime);   	  	
    	Main.setTimeStamps(timeStamp);
	  	 
	       	//System.out.println(sb.toString());
	  		System.out.println("message received from client :\n"+obj);
	}

}
