package server;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class Jobs_Keeper {
	
	private List<JSONObject> list = new ArrayList<JSONObject>();
	private DB_Jobs db_jobs = new DB_Jobs(Main.getDb_credentials());
	
	/**
	 * Constructor of jobs keeper
	 */
	public Jobs_Keeper(){
		
	}
	
	/**
	 * refreshing the jobs keeper list by deleting sent jobs
	 * @param sa_id
	 */
	private void refresh_list(String sa_id){
		for(int i=0;i<this.list.size();i++){
			JSONObject job_to_clean = this.list.get(i);
    		String sa_id_to_rm = (String) job_to_clean.get("sa_id");
    		if(sa_id_to_rm.equals(sa_id)){
    			this.list.remove(job_to_clean);
    		}
		}
	}
	
	/**
	 * putting a new job to be sent in short time
	 * @param job : holds the job to be sent to the SA
	 */
	public synchronized void putJob(JSONObject job){
		this.list.add(job);
	}
	
	/**
	 * getting all jobs for a SA
	 * @param sa_id : id of sa to search jobs for
	 * @return
	 */
	public synchronized List<JSONObject> getJobs(String sa_id){
		List<JSONObject> jobs_to_send = new ArrayList<JSONObject>();
		for(int i=0;i<this.list.size();i++){
			JSONObject job_to_send = this.list.get(i);
			String sa = (String) job_to_send.get("sa_id");
    		if(sa.equals(sa_id)){
    			jobs_to_send.add(job_to_send);
    		}
		}
		refresh_list(sa_id);
		return jobs_to_send;
	}
	

}
