
package server;

import java.awt.EventQueue;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import javax.swing.JOptionPane;
import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;

import GUI.ConnectToDB;
import GUI.GUI;
import cache.Jobs_Keeper;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;

import database.DBUsers;
import database.DB_Credentials;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class Main {
	
	// Database driver and URL
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost:3306/serverdata";
			
	//Database credentials
	private static final String USER = "root";
	private static final String PASSWORD = "nektarios6377";
	
	private static DB_Credentials db_credentials;
	
	public static DB_Credentials getDb_credentials() {
		return db_credentials;
	}

	private static DBUsers db_users;
	
	public static DBUsers getDb_users() {
		return db_users;
	} 
	
	private static BlockingQueue continue_exec = new ArrayBlockingQueue(1);
	
	
	public static BlockingQueue getContinue_exec() {
		return continue_exec;
	}

	private static List<String> hash_key_table = new ArrayList<String>();
	
	public static List<String> getHash_key_table() {
		return hash_key_table;
	}
	
	public static void remove_sa_key(String sa_rm){
		Main.hash_key_table.remove(sa_rm);
	}
	
	private static String sa_to_send_job = new String();
	
	public static String getSa_to_send_job() {
		return sa_to_send_job;
	}

	public static void setSa_to_send_job(String sa_to_send_job) {
		Main.sa_to_send_job = sa_to_send_job;
	}
	
	private static String sa_to_remove = new String();

	public static String getSa_to_remove() {
		return sa_to_remove;
	}

	public static void setSa_to_remove(String sa_to_remove) {
		Main.sa_to_remove = sa_to_remove;
	}

	private static BlockingQueue<String> accept_sa = new ArrayBlockingQueue<String>(1);

	public static String getAccept_sa() throws InterruptedException {
		String s = accept_sa.poll(20, TimeUnit.SECONDS);
		accept_sa.clear();
		return s;
	}

	public static void setAccept_sa(String accept_sa) {
		Main.accept_sa.add(accept_sa);
	}
	
	private static BlockingQueue<String> accept_android = new ArrayBlockingQueue<String>(1);

	public static BlockingQueue<String> getAccept_android() {
		return accept_android;
	}

	public static void setAccept_android(BlockingQueue<String> accept_android) {
		Main.accept_android = accept_android;
	}

	
	public static void setHash_key_table(String new_hash_key) {
		Main.hash_key_table.add(new_hash_key);
		System.out.println("hash key :"+new_hash_key+" added!");
	}
	
	private static Jobs_Keeper jobs_keeper;

	public static Jobs_Keeper getJobs_keeper() {
		return jobs_keeper;
	}

	public static void setJobs_keeper(Jobs_Keeper jobs_keeper) {
		Main.jobs_keeper = jobs_keeper;
	}
	
	private static ArrayList<Long> timeStamps = new ArrayList<Long>();

	public static ArrayList<Long> getTimeStamps() {
		return timeStamps;
	}

	public static void setTimeStamps(ArrayList<Long> timeStamps) {
		Main.timeStamps = timeStamps;
	}

	//public static int counter = 0;
	public static BlockingQueue<Integer> counter = new ArrayBlockingQueue<Integer>(1);
	
	public static void IncrementCounter() throws InterruptedException {
		int i = Main.counter.take();
		i++;
		Main.counter.add(i);
	}

	private static int getPort(int defaultPort) {
        //grab port from environment, otherwise fall back to default port 9998
        String httpPort = System.getProperty("jersey.test.port");
        if (null != httpPort) {
            try {
                return Integer.parseInt(httpPort);
            } catch (NumberFormatException e) {
            }
        }
        return defaultPort;
    }

    private static URI getBaseURI() {
    	ReadPropertyFile pr = new ReadPropertyFile("config.properties");
    	String ip = pr.getIP();
    	int port = pr.getPORT();
        //return UriBuilder.fromUri("http://localhost/").port(getPort(8080)).build();
    	return UriBuilder.fromUri("http://"+ip+"/").port(getPort(port)).build();
    }

    public static final URI BASE_URI = getBaseURI();
    
    protected static HttpServer startServer() throws IOException {
    	//WARNING!
    	//HERE YOU NAME THE PACKAGE IN WHICH THE WEB RESOURCES ARE IMPLEMENTED!
    	//in this case it is the package webresources
        ResourceConfig resourceConfig = new PackagesResourceConfig("webresources");

        System.out.println("Starting grizzly2...");
        return GrizzlyServerFactory.createHttpServer(BASE_URI, resourceConfig);
    }
    
    /**
     * Main function
     * @param args
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException, ClassNotFoundException, InterruptedException {
    	
    	Class.forName("com.mysql.jdbc.Driver");
    	
    	db_credentials = new DB_Credentials(DB_URL,USER,PASSWORD);
    	
    	db_users = new DBUsers(db_credentials);
    	
    	System.out.println(db_users.getNamePass(USER));
    	
    	//must set up connection to DB
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ConnectToDB window = new ConnectToDB();
					window.getPass().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    	
		boolean to_continue =  (Boolean) continue_exec.take();
		
		System.out.println(to_continue);
		
		if(to_continue == false){
			return;
		}
		
		jobs_keeper = new Jobs_Keeper();
    	
        // Grizzly 2 initialization
        HttpServer httpServer = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\n",BASE_URI));
        
        Main.counter.put(0);
		
        
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
        
        System.in.read();
        httpServer.stop();
        
        db_users.close_database();
    }

	public static int getCounter() {
		return counter.peek();
	}
   
}
