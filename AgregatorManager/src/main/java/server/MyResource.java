
package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.lang.String;
import javax.ws.rs.core.Response;


import org.json.simple.JSONObject;

// The Java class will be hosted at the URI path "/myresource"
@Path("/test")
public class MyResource {

    // TODO: update the class to suit your needs
    
    // The Java method will process HTTP GET requests
    @GET 
    @Path("/output")
    @Produces(MediaType.APPLICATION_JSON)
    public String getIt() {
  	  JSONObject obj=new JSONObject();
  	  obj.put("name","foo");
  	  System.out.print(obj);
  	  //return Response.status(200).entity(obj.toJSONString()).build();
  	  return obj.toJSONString();
    }
    
    


 @PUT
 @Consumes(MediaType.APPLICATION_JSON)
 @Path("/result")
 public void putIntoDAO(InputStream xml) {
 	String line = "";
   	StringBuilder sb = new  StringBuilder();
 	JSONObject obj=new JSONObject();
  	try {
       	BufferedReader br = new BufferedReader(new InputStreamReader(xml));
       	while ((line = br.readLine()) != null) {
           	sb.append(line);
       	}
       	} catch (IOException e) {
          	e.printStackTrace();
       	}
  		obj.put("mesage",sb.toString());
       	//System.out.println(sb.toString());
  		System.out.println("message received from client :\n"+obj);
        }
    
}
