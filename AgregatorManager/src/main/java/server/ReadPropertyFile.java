package server;

import java.io.*;
import java.util.Properties;

/**
* 
* Class ReadPropertyFile reads the handles the .congig files
*
*/
public class ReadPropertyFile {
	
	private Properties prop = null;
	private FileReader reader ;
	
	/**
	 * constructor opens necessary config file 
	 * @param path : name of file
	 */
	public ReadPropertyFile(String path){
		File fl = new File(path);
		this.prop = new Properties();
		
		try {
			this.reader = new FileReader(fl) ;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		try {
			prop.load(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * @return : number of threads for thread pool
	 */
	public String getIP(){
		String str = prop.getProperty("IP");
		return str;
	}
	
	/**
	 * 
	 * @return value for main to sleep
	 */
	public int getPORT(){
		String str = prop.getProperty("PORT");
		return Integer.parseInt(str);
	}

}
