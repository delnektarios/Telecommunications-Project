
package server;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
@Path("/register")
public class Register {
	
	private String sa = new String();
	private String key = new String();
	private StringBuilder sb = new  StringBuilder();
	private Registration register = new Registration();

	
    // The Java method will process HTTP GET requests
    @GET 
    @Path("/answer")
    @Produces(MediaType.APPLICATION_JSON)
    public String PutOrNotIntoDAO() throws ParseException, InterruptedException {
  	  JSONObject obj=new JSONObject();
  	  //obj.put("answer","permision denied");
  	  System.out.println(sa);
  	  System.out.println(key);
  	  if(sa.equals(key)){
  		obj.put("answer","permision granted");
    	Long currentTime = System.currentTimeMillis();
    	ArrayList<Long> timeStamp = Main.getTimeStamps();
    	timeStamp = Main.getTimeStamps();
    	timeStamp.add(currentTime);  	  	
    	Main.setTimeStamps(timeStamp);
  	  }
  	  else{
  		obj.put("answer","permision denied");
  	  }
  	  System.out.print(obj);
  	  //return Response.status(200).entity(obj.toJSONString()).build();
  	  return obj.toJSONString();
    }

    public Registration getRegister() {
		return register;
	}


	@PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/question")
    public void AcceptSA(InputStream xml) throws ParseException, InterruptedException {
    	String line = "";
    	try {
    		BufferedReader br = new BufferedReader(new InputStreamReader(xml));
    		while ((line = br.readLine()) != null) {
    			this.sb.append(line);
    		}
       		} catch (IOException e) {
       			e.printStackTrace();
       		}
    		
    	EventQueue.invokeLater(new Runnable() {
    		public void run() {
    			try {
    				Accept_SAs window = new Accept_SAs(sb.toString());
    				window.getFrame().setVisible(true);
    			} catch (Exception e) {
    				e.printStackTrace();
    			}
    		}
    	});
    	
    	if(!(Main.getAccept_sa().isEmpty())){
    	 	System.out.println("time to register");	
    		register.add_SA(sb.toString());
    	  	  sa = Main.getAccept_sa();
    	  	  key = register.getHash_key();
    	}
    	
    }
	
    
}
