package server;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * @author Nektarios Deligiannakis 1115201200030
 * @author Fwkiwn Zikidis 111201100037
 *
 */
public class Registration {
	
	private List<String> SA_specs = new ArrayList<String>();
	
	private RegistrationForm sa_form;
	private DB_SA_Identify db_sa_identify;
	
	private String Device_Name;
	private String Interface_IP;
	private String Interface_MacAddr;
	private String Os_Version;
	private String NMap_Version;
	private String Hash_key;
	
	/**
	 * Registering SA to db
	 * @param SA_specs : SA specifications or info
	 * @throws ParseException
	 */
	public void add_SA(String SA_specs) throws ParseException{
		//parsing specs and determining acception of request
		//if not accepted set flag to 0;
		//else
		JSONObject obj = (JSONObject) new JSONParser().parse(SA_specs);
    	this.Device_Name = (String) obj.get("Device_Name");
    	this.Interface_IP = (String) obj.get("Interface_IP");
    	this.Interface_MacAddr = (String) obj.get("Interface_MacAddr");
    	this.Os_Version = (String) obj.get("Os_Version");
    	this.NMap_Version = (String) obj.get("NMap_Version");
    	this.Hash_key = (String) obj.get("Hash_key");
    	
    	System.out.println(this.Device_Name + this.Interface_IP + this.Interface_MacAddr + this.Os_Version + this.NMap_Version);
    	
    	sa_form = new RegistrationForm(this.Device_Name,this.Interface_IP,this.Interface_MacAddr,this.Os_Version, this.NMap_Version,this.Hash_key);
    	
    	db_sa_identify = new DB_SA_Identify(Main.getDb_credentials());
    	db_sa_identify.insertSAform(sa_form);
    	db_sa_identify.close_database();
    	
	}

	public String getHash_key() {
		return Hash_key;
	}

	public void setHash_key(String hash_key) {
		Hash_key = hash_key;
	}

	public String getDevice_Name() {
		return Device_Name;
	}


	public String getInterface_IP() {
		return Interface_IP;
	}

	public String getInterface_MacAddr() {
		return Interface_MacAddr;
	}

	public String getOs_Version() {
		return Os_Version;
	}

	public String getNMap_Version() {
		return NMap_Version;
	}

	public List<String> getSA_specs() {
		return SA_specs;
	}

}
