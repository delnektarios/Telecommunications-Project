package server;

public class RegistrationForm {
	
	String Device_Name;
	String Interface_IP;
	String Interface_MacAddr;
	String Os_Version;
	String NMap_Version;
	String Hash_key;
	private boolean terminated;
	
	public RegistrationForm() { 
		
	}
	
	/**
	 * Creating a new form to keep all SA info together in an object
	 * @param Device_Name
	 * @param Interface_IP
	 * @param Interface_MacAddr
	 * @param Os_Version
	 * @param NMap_Version
	 * @param Hash_key
	 */
	public RegistrationForm(String Device_Name,String Interface_IP,
						String Interface_MacAddr,String Os_Version, 
						String NMap_Version,String Hash_key){
		this.Device_Name = Device_Name;
		this.Interface_IP = Interface_IP;
		this.Interface_MacAddr = Interface_MacAddr;
		this.Os_Version = Os_Version;
		this.NMap_Version = NMap_Version;
		this.Hash_key = Hash_key;
		this.setTerminated(false);
	}

	public String getDevice_Name() {
		return Device_Name;
	}

	public void setDevice_Name(String device_Name) {
		Device_Name = device_Name;
	}

	public String getInterface_IP() {
		return Interface_IP;
	}

	public void setInterface_IP(String interface_IP) {
		Interface_IP = interface_IP;
	}

	public String getInterface_MacAddr() {
		return Interface_MacAddr;
	}

	public void setInterface_MacAddr(String interface_MacAddr) {
		Interface_MacAddr = interface_MacAddr;
	}

	public String getOs_Version() {
		return Os_Version;
	}

	public void setOs_Version(String os_Version) {
		Os_Version = os_Version;
	}

	public String getNMap_Version() {
		return NMap_Version;
	}

	public void setNMap_Version(String nMap_Version) {
		NMap_Version = nMap_Version;
	}

	public String getHash_key() {
		return Hash_key;
	}

	public void setHash_key(String hash_key) {
		Hash_key = hash_key;
	}

	public boolean isTerminated() {
		return terminated;
	}

	public void setTerminated(boolean terminated) {
		this.terminated = terminated;
	}

}
