package webresources;

import java.awt.EventQueue;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import cache.Jobs_Keeper;
import database.DBUsers;
import database.DB_Jobs;
import database.DB_Results;
import database.DB_SA_Identify;
import database.Nmap_job;
import database.RegistrationForm;
import server.Main;
import database.SA_Results;
import GUI.GUI;
import GUI.RegisterNewAndroidClient;


@Path("/android")
public class androidWebResources {

    
	 @PUT
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Path("/storeUser")
	 public Response storeUser(InputStream xml) throws ParseException {
		 
		 String line = "";
		 StringBuilder sb = new  StringBuilder();
	  	 try {
	       	BufferedReader br = new BufferedReader(new InputStreamReader(xml));
	       	while ((line = br.readLine()) != null) {
	           	sb.append(line);
	       	}
	       	} catch (IOException e) {
	          	e.printStackTrace();
	       	}
	  	 
	  	final JSONObject obj = (JSONObject) new JSONParser().parse(sb.toString());
    	System.out.println("in the web service android");
    	System.out.println(obj.get("name"));
    	System.out.println(obj.get("service"));
    	System.out.println(obj.get("username"));
    	System.out.println(obj.get("password"));
    	
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					RegisterNewAndroidClient window = new RegisterNewAndroidClient(obj);
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		return Response.status(200).build();
	}
    
    @GET 
    @Path("/fetchUser/{username}/{password}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response returnAndroidUser(@PathParam("username") String username, @PathParam("password") String password){
 
    	JSONObject a = new JSONObject();
    	
    	System.out.println("in the web service android to fetch user");
    	System.out.println(username);
    	System.out.println(password);
    	
    	//must fetch user from database
    	DBUsers db_users = new DBUsers(Main.getDb_credentials());
    	boolean accept = db_users.validatePassword(username, password);
    	//if the user exists send user details
    	if (accept){
        	a.put("name", "valid");
        	a.put("service", "register");
        	a.put("registered", true);
        	JOptionPane.showMessageDialog(null, "WARNING! Android client is authorised to manage Software Agents");
    	}
    	//else return "user does not exist"
    	else{
        	a.put("name", "invalid");
        	a.put("service", "register");
        	a.put("registered", false);
        	JOptionPane.showMessageDialog(null, "Android client login task failed due to incorrect entry!");
    	}
    	
    	return Response.status(200).entity(a.toString()).build();
    }
    
    @GET 
    @Path("/fetchSAs")
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchSAs() {
    	
    	DB_SA_Identify db_sa_identify = new DB_SA_Identify(Main.getDb_credentials());
    	    	    	
    	StringBuilder str = new StringBuilder();
    	
    	JSONArray array = new JSONArray();
    	
		for(int i=0;i<Main.getHash_key_table().size();i++){
			System.out.println("----IN GUI------");
			System.out.println(Main.getHash_key_table().get(i));
			System.out.println("----IN GUI OUT------");
			RegistrationForm sa_form = db_sa_identify.getSAform(Main.getHash_key_table().get(i));
			str.append(sa_form.getDevice_Name().toString()+"\n");
			JSONObject obj = new JSONObject();
			obj.put("SA", sa_form.getDevice_Name());
			array.put(obj);
		}
		
		db_sa_identify.close_database();
		
    	System.out.println(array.toString());
    	//return obj.toJSONString();
    	return Response.status(200).entity(array.toString()).build();
    }
    
    @GET 
    @Path("/fetchSAResults/{position}/{number}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchSAResults(@PathParam("position") String position, @PathParam("number") String number) {
    	
    	System.out.println(position +" ; "+number);
    	
    	JSONArray array = new JSONArray();
    	
    	DB_Results db_results = new DB_Results(Main.getDb_credentials());

		List<SA_Results> all_per_sa = db_results.findResults(Main.getHash_key_table().get(Integer.parseInt(position)));
		System.out.println("siza : "+all_per_sa.size());
		int toTake = all_per_sa.size()-1-Integer.parseInt(number);
		if(toTake < -1){
			toTake = -1;
		}
		System.out.println("to take: " +toTake );
		for(int i=all_per_sa.size()-1;i>toTake;i--){
			JSONObject obj = new JSONObject();
			obj.put("scanner", all_per_sa.get(i).getResults());
			obj.put("command arguments", all_per_sa.get(i).getArgument1());
			obj.put("start time", all_per_sa.get(i).getArgument2());
			obj.put("protocol", all_per_sa.get(i).getArgument3());
			obj.put("summary", all_per_sa.get(i).getArgument4());
			
			array.put(obj);
		}
    	
		db_results.close_database();
		return Response.status(200).entity(array.toString()).build();
    }
    
    
    @GET 
    @Path("/fetchAllResults/{number}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response fetchAllResults(@PathParam("number") String number) {
    	
    	int numberOfResults = Integer.parseInt(number);
    	DB_Results db_results = new DB_Results(Main.getDb_credentials());
    	
    	JSONArray array = new JSONArray();
    	
		for(int j=0;j<Main.getHash_key_table().size();j++){
			 List<SA_Results> all_per_sa = db_results.findResults(Main.getHash_key_table().get(j));
				int toTake = all_per_sa.size()-1-Integer.parseInt(number);
				if(toTake < -1){
					toTake = -1;
				}
			 for(int i=all_per_sa.size()-1;i>toTake;i--){
					JSONObject obj = new JSONObject();
					obj.put("scanner", all_per_sa.get(i).getResults());
					obj.put("command arguments", all_per_sa.get(i).getArgument1());
					obj.put("start time", all_per_sa.get(i).getArgument2());
					obj.put("protocol", all_per_sa.get(i).getArgument3());
					obj.put("summary", all_per_sa.get(i).getArgument4());
					array.put(obj);	 
			 }
		}
    	
		db_results.close_database();
		return Response.status(200).entity(array.toString()).build();
    }
    
    @GET 
    @Path("/TerminateSA/{position}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response TerminateSA(@PathParam("position") String position) {
    	    	
    	JSONObject obj = new JSONObject();
    	obj.put("SATerminated", Main.getHash_key_table().get(Integer.parseInt(position)));
    	
		JSONObject term = new JSONObject();
		term.put("id", "-1");
		term.put("parameters", "exit(0)");
		term.put("periodic", "true");
		term.put("time", "-1");
		term.put("sa_id",Main.getHash_key_table().get(Integer.parseInt(position)));
		
		Jobs_Keeper jobs_keeper = jobs_keeper = Main.getJobs_keeper();
		jobs_keeper.putJob(term);
		
		DB_SA_Identify db_sa_identify = new DB_SA_Identify(Main.getDb_credentials());
		db_sa_identify.updateTerminated(Main.getHash_key_table().get(Integer.parseInt(position)));
		Main.setSa_to_remove(Main.getHash_key_table().get(Integer.parseInt(position)));
		
		db_sa_identify.close_database();
		return Response.status(200).entity(obj.toString()).build();
    }
    
	 @PUT
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Path("/AcceptJob")
	 public Response AcceptJob(InputStream xml) throws ParseException {
		 
		 List<Integer> periodic_done = new ArrayList<Integer>();
		 
		 String line = "";
		 StringBuilder sb = new  StringBuilder();
	  	 try {
	       	BufferedReader br = new BufferedReader(new InputStreamReader(xml));
	       	while ((line = br.readLine()) != null) {
	           	sb.append(line);
	       	}
	       	} catch (IOException e) {
	          	e.printStackTrace();
	       	}
	  	 
	  	JSONObject obj = (JSONObject) new JSONParser().parse(sb.toString());
	  	int position = Integer.parseInt((String) obj.get("position"));
		
		DB_Jobs db_jobs = new DB_Jobs(Main.getDb_credentials());
		
		Nmap_job jobDB = new Nmap_job();

		try {
			//FOR THE UNIQUE IDs OF JOBS
			Main.IncrementCounter();
			jobDB.setIdNMap(Main.getCounter());
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		jobDB.setCommand((String) obj.get("parameters"));
		if(obj.get("periodic").equals("true")){
			jobDB.setPeriodic(true);
			periodic_done = GUI.getPeriodic_done();
			periodic_done.add(0);
			GUI.setPeriodic_done(periodic_done);
		}
		else{
			jobDB.setPeriodic(false);
		}
		jobDB.setPeriod(Integer.parseInt((String) obj.get("time")));
		jobDB.setSA_Hashkey(Main.getHash_key_table().get(position));
		
		java.util.Date date = new java.util.Date();
		java.sql.Date jobtime = convertJavaDateToSqlDate(date);
		
		jobDB.setJobtime(jobtime);
		db_jobs.insertJob(jobDB);
	  	
	  	System.out.println("object received: "+obj.toString());
	  	
		JSONObject job = new JSONObject();
		
		job.put("id", Integer.toString(jobDB.getIdNMap()));
		
		job.put("parameters", (String) obj.get("parameters"));
		job.put("periodic", (String) obj.get("periodic"));
		job.put("time", (String) obj.get("time"));
		job.put("sa_id",Main.getHash_key_table().get(position));
		
		System.out.println("object received: "+job.toString());
		
		Jobs_Keeper jobs_keeper = jobs_keeper = Main.getJobs_keeper();
		jobs_keeper.putJob(job); 
		db_jobs.close_database();
		
		return Response.status(200).build();
	}
	 
		private java.sql.Date convertJavaDateToSqlDate(java.util.Date date) {
		    return new java.sql.Date(date.getTime());
		}
	 
	    @GET 
	    @Path("/getPeriodics")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response getPeriodics() {
	    	
	    	JSONArray array = new JSONArray();
	    	
	    	DB_Jobs db_jobs = new DB_Jobs(Main.getDb_credentials());
	    	
	    	//List<Integer> id_list = new ArrayList<Integer>();
	    	
	    	List<Integer> periodic_done = new ArrayList<Integer>();
	    	
			periodic_done = GUI.getPeriodic_done();
	    	
			List<Nmap_job> periodic_jobs = new ArrayList<Nmap_job>();
			periodic_jobs = db_jobs.findPeriodicJob();
			
			if(periodic_done.size() == 0){
				JSONObject obj = new JSONObject();
				obj.put("nothing", "nothing");
				return Response.status(200).entity(obj.toString()).build();
			}
			
			//GETTING THE ID OF THE PERIODIC JOB FOR EACH PERIODIC JOB
			for(int i=0;i<periodic_jobs.size();i++){
				if(periodic_done.get(i) == 0){
					JSONObject obj = new JSONObject();
					obj.put("parameters", periodic_jobs.get(i).getCommand());
					array.put(obj);
				}
			}
			
			db_jobs.close_database();
			return Response.status(200).entity(array.toString()).build();
	    }
	    
	    @GET 
	    @Path("/terminatePeriodic/{position}")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response terminatePeriodic(@PathParam("position") String position) {
	    		    	
	    	DB_Jobs db_jobs = new DB_Jobs(Main.getDb_credentials());
	    	
	    	List<Integer> periodic_done = new ArrayList<Integer>();
	    	
			periodic_done = GUI.getPeriodic_done();
	    	
			List<Nmap_job> periodic_jobs = new ArrayList<Nmap_job>();
			periodic_jobs = db_jobs.findPeriodicJob();
			//GETTING THE ID OF THE PERIODIC JOB FOR EACH PERIODIC JOB
			
			Nmap_job job = periodic_jobs.get(Integer.parseInt(position));
			int id = job.getIdNMap();
			String hashkey = job.getSA_Hashkey();
			
			JSONObject obj = new JSONObject();
			obj.put("id", Integer.toString(id));
			System.out.println("id of periodic to terminate : "+id);
			obj.put("parameters", "Stop");
			obj.put("periodic", "true");
			obj.put("time", "periodic");
			obj.put("sa_id",hashkey);
			//add job to the job keeper 
			Jobs_Keeper jobs_keeper = Main.getJobs_keeper();
			jobs_keeper.putJob(obj);
			
			periodic_done.add(Integer.parseInt(position), 1);
			GUI.setPeriodic_done(periodic_done);
					
			JSONObject done = new JSONObject();
			obj.put("done position", position);
			db_jobs.close_database();
			
			return Response.status(200).entity(done.toString()).build();
	    }
	    
	    @GET 
	    @Path("/fetchAllJobs")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response fetchAllJobs() {
	    		    	
	    	DB_Jobs db_jobs = new DB_Jobs(Main.getDb_credentials());
	    	
	    	JSONArray array = new JSONArray();
	    	
	    	for (int i=0;i<Main.getHash_key_table().size();i++){
	    		List<Nmap_job> jobs = db_jobs.findJobs(Main.getHash_key_table().get(i));
	    		for (int j=0;j<jobs.size();j++){
	    			JSONObject job = new JSONObject();
	    			
	    			job.put("id", Integer.toString(jobs.get(j).getIdNMap()));
	    			
	    			job.put("parameters", jobs.get(j).getCommand());
	    			if(jobs.get(j).isPeriodic() == true){
	    				job.put("periodic", "true");
	    			}else if(jobs.get(j).isPeriodic() == false){
	    				job.put("periodic", "false");
	    			}	    			
	    			job.put("time", Integer.toString(jobs.get(j).getPeriod()));
	    			
	    			array.put(job);
	    			
	    		}
	    	}
	    	db_jobs.close_database();

			return Response.status(200).entity(array.toString()).build();
	    }
	    
	    
	    @GET 
	    @Path("/fetchSAsStatus")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response fetchSAsStatus() {
	    		    		    	
	    	JSONArray array = new JSONArray();
	    	
	    	long currentTimeStamp = System.currentTimeMillis();
	    	
	    	DB_SA_Identify db_sa_identify = new DB_SA_Identify(Main.getDb_credentials());
	    	
	    	for(int i=0;i<Main.getHash_key_table().size();i++){
	    		
	    		StringBuilder str = new StringBuilder();
	    		RegistrationForm sa_form = db_sa_identify.getSAform(Main.getHash_key_table().get(i));
	    		str.append(sa_form.getDevice_Name().toString());
	
	    		//TIME INTERVAL SHOULD BE READ FROM A PROPERTY FILE
	    		if( currentTimeStamp - Main.getTimeStamps().get(i) < 10*1000){
	    			str.append(" (online) ");
	    		}else{
	    			str.append(" (offline) ");
	    		}
	    		
	    		JSONObject obj  = new JSONObject();
	    		obj.put("status", str.toString());
	    		array.put(obj);
	    	}
	    	

			return Response.status(200).entity(array.toString()).build();
	    }
	  
}
