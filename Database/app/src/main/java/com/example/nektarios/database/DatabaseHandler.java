package com.example.nektarios.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nektarios on 1/27/16.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "offlineJobs";
    private static final int DATABASE_VERSION = 5;

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_PARAMETERS = "parameters";
    private static final String KEY_IP = "IP";
    private static final String KEY_PERIODIC = "periodic";
    private static final String KEY_TIME = "time";
    private static final String KEY_POSITION = "position";



    // Contacts table name
    private static final String TABLE_JOBS = "offlinejobs";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_JOBS_TABLE = "CREATE TABLE " + TABLE_JOBS + "(" + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_PARAMETERS + " TEXT, " + KEY_IP + " TEXT, "
                + KEY_PERIODIC + " TEXT, " + KEY_TIME + " TEXT, " + KEY_POSITION + " TEXT "  + ")";
        db.execSQL(CREATE_JOBS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOBS);

        // Create tables again
        onCreate(db);
    }

    public void addJob(Jobs jobs) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PARAMETERS, jobs.getParameters());
        values.put(KEY_IP, jobs.getIP());
        values.put(KEY_PERIODIC, jobs.getPeriodic());
        values.put(KEY_TIME, jobs.getTime());
        values.put(KEY_POSITION, jobs.getPosition());


        // Inserting Row
        db.insert(TABLE_JOBS, null, values);
        db.close(); // Closing database connection
    }


    public List<Jobs> getAllJobs() {
        List<Jobs> jobsList = new ArrayList<Jobs>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_JOBS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                //Contact contact = new Contact();
                Jobs jobs = new Jobs();
                jobs.setParameters(cursor.getString(1));
                jobs.setIP(cursor.getString(2));
                jobs.setPeriodic(cursor.getString(3));
                jobs.setTime(cursor.getString(4));
                jobs.setPosition(cursor.getString(5));
                // Adding contact to list
                jobsList.add(jobs);
            } while (cursor.moveToNext());
        }
        return jobsList;
    }

}
