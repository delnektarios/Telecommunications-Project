package com.trito_paradoteo.project.myapplications;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public interface GetServerCallBack {
    public abstract void done(ArrayList<String> arrayList);
    public abstract void done(StringBuilder stringBuilder);
    public abstract void done(JSONArray jsonArray);
    public abstract void done();
}
