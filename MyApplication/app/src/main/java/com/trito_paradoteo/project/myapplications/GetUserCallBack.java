package com.trito_paradoteo.project.myapplications;

import com.trito_paradoteo.project.myapplications.users.User;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public interface GetUserCallBack {
    public abstract void done(User returnedUser);
}
