package com.trito_paradoteo.project.myapplications.activities;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trito_paradoteo.project.myapplications.GetUserCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.fragments.Settings;
import com.trito_paradoteo.project.myapplications.reguest.server.ServerRequests;
import com.trito_paradoteo.project.myapplications.users.User;
import com.trito_paradoteo.project.myapplications.users.UserLocalStore;

public class Login extends AppCompatActivity implements View.OnClickListener {

    Button login;
    EditText editUsername, editPassword, editIP;
    TextView textToRegister;
    UserLocalStore userLocalStore;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        login = (Button) findViewById(R.id.login);
        editUsername = (EditText) findViewById(R.id.editUsername);
        editPassword = (EditText) findViewById(R.id.editPassword);
        editIP = (EditText) findViewById(R.id.editIP1);
        textToRegister = (TextView) findViewById(R.id.textToRegister);

        login.setOnClickListener(this);
        textToRegister.setOnClickListener(this);
        userLocalStore = new UserLocalStore(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.login:
                User user = null;
                //validating username and password with database
                String username = editUsername.getText().toString();
                String password = editPassword.getText().toString();

                Settings.com_ip = editIP.getText().toString();
                if (Settings.com_ip.isEmpty()){
                    Settings.com_ip = Settings.DEFAULT_IP;
                }

                user = new User(username,password);

                authenticateUser(user);

                userLocalStore.storeUserData(user);
                userLocalStore.setUserLoggedIn(true);
                break;
            case R.id.textToRegister:
                startActivity(new Intent(this, Register.class));

                break;
        }
    }

    /**
     * method that checks whether a user is still logged on
     * @param user
     */
    private void authenticateUser(User user){
        ServerRequests serverRequests = new ServerRequests(this);
        serverRequests.fetchUserInBackground(user, new GetUserCallBack() {
            @Override
            public void done(User returnedUser) {
                if(returnedUser == null){
                    errorMessageDialog();
                }else{
                    loggingUserIn(returnedUser);
                }
            }
        });
    }

    /**
     * prints error message in case of unsuccessfull login due to incorect entry
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(Login.this);
        dialogBuilder.setMessage("Incorrect Entry!");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

    /**
     * storing the details of a user in shared preferences
     * @param returnedUser
     */
    private void loggingUserIn(User returnedUser){
        userLocalStore.storeUserData(returnedUser);
        userLocalStore.setUserLoggedIn(true);

        startActivity(new Intent(this, MainActivity.class));
    }
}
