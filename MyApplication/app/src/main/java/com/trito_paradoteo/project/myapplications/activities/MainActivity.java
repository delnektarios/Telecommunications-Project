package com.trito_paradoteo.project.myapplications.activities;

/**
 *Nektarios Deligiannakis 1115201200030
 *Fwkiwnas Zikidis 1115201100037
 */

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.trito_paradoteo.project.myapplications.database.sqlite.DatabaseHandler;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.fragments.AllResultsFragment;
import com.trito_paradoteo.project.myapplications.fragments.DeletePeriodicFragment;
import com.trito_paradoteo.project.myapplications.fragments.EnteringNmapJobsFragment;
import com.trito_paradoteo.project.myapplications.fragments.ResultsPerSAFRagment;
import com.trito_paradoteo.project.myapplications.fragments.SATerminationFragment;
import com.trito_paradoteo.project.myapplications.fragments.SA_status_fragment;
import com.trito_paradoteo.project.myapplications.fragments.Settings;
import com.trito_paradoteo.project.myapplications.users.UserLocalStore;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static DatabaseHandler databaseHandler;
    public static List<Integer> index;
    private UserLocalStore userLocalStore;

    private ActionBar actionBar;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private ListView listView;
    private DrawerLayout drawerLayout;

    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;

    @Override
    protected void onStart() {
        super.onStart();
        if(!(authenticateUserLoggedIn())){
            startActivity(new Intent(this, Login.class));
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        databaseHandler = new DatabaseHandler(this);
        databaseHandler.deleteAllRecords();
        //databaseHandler.deleteRecords();
        //index  = new ArrayList<Integer>();
        userLocalStore = new UserLocalStore(this);

        //fragment drawer
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        listView = (ListView) findViewById(R.id.navigationlist);
        ArrayList<String> arrayList = new ArrayList<>();
        //adding fragment titles
        arrayList.add("Results per SA");
        arrayList.add("All Results");
        arrayList.add("Delete Periodic");
        arrayList.add("Nmap Jobs");
        arrayList.add("SA status");
        arrayList.add("Terminate SAs");
        arrayList.add("Settings");
        arrayList.add("Logout");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_activated_1, arrayList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.opendrawer, R.string.closedrawer);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //hamburger menu
        actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        fragmentManager = getSupportFragmentManager();
        //default selection for drawer when starting the app
        setDefaultListSelectionFragment();
    }

    /**
     * setting the default fragment to show as a first choice
     */
    private void setDefaultListSelectionFragment(){
        listView.setItemChecked(0, true);
        ResultsPerSAFRagment resultsPerSAFRagment = new ResultsPerSAFRagment();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentholder, resultsPerSAFRagment);
        fragmentTransaction.commit();

    }

    private void loadListSelection(int i){
        listView.setItemChecked(i, true);
        switch(i){
            case 0:
                ResultsPerSAFRagment resultsPerSAFRagment = new ResultsPerSAFRagment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, resultsPerSAFRagment);
                fragmentTransaction.commit();
                break;
            case 1:
                AllResultsFragment allResultsFragment = new AllResultsFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, allResultsFragment);
                fragmentTransaction.commit();
                break;
            case 2:
                DeletePeriodicFragment deletePeriodicFragment = new DeletePeriodicFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, deletePeriodicFragment);
                fragmentTransaction.commit();
                break;
            case 3:
                EnteringNmapJobsFragment enteringNmapJobsFragment = new EnteringNmapJobsFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, enteringNmapJobsFragment);
                fragmentTransaction.commit();
                break;
            case 4:
                SA_status_fragment sa_status_fragment = new SA_status_fragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, sa_status_fragment);
                fragmentTransaction.commit();
                break;
            case 5:
                SATerminationFragment saTerminationFragment = new SATerminationFragment();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, saTerminationFragment);
                fragmentTransaction.commit();
                break;
            case 6:
                Settings settings = new Settings();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, settings);
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if(id == R.id.action_settings){
            return true;
        }else if(id == android.R.id.home){
            if(drawerLayout.isDrawerOpen(listView)){
                drawerLayout.closeDrawer(listView);
            }else{
                drawerLayout.openDrawer(listView);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean authenticateUserLoggedIn(){
        return userLocalStore.getUserLoggedIn();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        switch(position){
            case 7:
                //logging out user
                userLocalStore.clearUsersData();
                userLocalStore.setUserLoggedIn(false);
                databaseHandler.deleteAllRecords();
                //databaseHandler.deleteRecords();
                //databaseHandler.reset();
                startActivity(new Intent(this, Login.class));
                break;
            default:
                loadListSelection(position);
                break;
        }
        drawerLayout.closeDrawer(listView);
    }
}
