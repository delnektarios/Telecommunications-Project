package com.trito_paradoteo.project.myapplications.activities;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.trito_paradoteo.project.myapplications.GetUserCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.fragments.Settings;
import com.trito_paradoteo.project.myapplications.reguest.server.ServerRequests;
import com.trito_paradoteo.project.myapplications.users.User;

public class Register extends AppCompatActivity implements View.OnClickListener {

    Button buttonRegister;
    EditText editRegisterName, editRegisterService, editRegisterUsername, editRegisterPassword, editIP;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        buttonRegister = (Button) findViewById(R.id.buttonRegister);

        editRegisterName = (EditText) findViewById(R.id.editRegisterName);
        editRegisterService = (EditText) findViewById(R.id.editRegisterService);
        editRegisterUsername = (EditText) findViewById(R.id.editRegisterUsername);
        editRegisterPassword = (EditText) findViewById(R.id.editRegisterPassword);
        editIP = (EditText) findViewById(R.id.editIP2);

        buttonRegister.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.buttonRegister:
                String name = editRegisterName.getText().toString();
                String service = editRegisterService.getText().toString();
                String username = editRegisterUsername.getText().toString();
                String passsword = editRegisterPassword.getText().toString();

                Settings.com_ip = editIP.getText().toString();
                if (Settings.com_ip.isEmpty()){
                    Settings.com_ip = Settings.DEFAULT_IP;
                }

                User registeringUserDetails = new User(name,service,username,passsword);

                registerUserWithDetails(registeringUserDetails);

                break;
        }
    }

    /**
     * registerinfg a new user
     * @param user
     */
    private void registerUserWithDetails(User user){
        ServerRequests serverRequests = new ServerRequests(this);
        serverRequests.storeUserInBackground(user, new GetUserCallBack() {
            @Override
            public void done(User returnedUser) {
                startActivity(new Intent(Register.this, Login.class));
            }
        });
    }
}
