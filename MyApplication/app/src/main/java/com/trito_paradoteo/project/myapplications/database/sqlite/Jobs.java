package com.trito_paradoteo.project.myapplications.database.sqlite;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

/**
 * class for jobs
 */
public class Jobs {

    private int id;
    private String parameters;
    private String IP;
    private String periodic;
    private String time;
    private String position;

    /**
     * creating a new job to interact with the database
     */
    public Jobs(){
    }

    /**
     * creating a new job to interact with the database
     * @param parameters the flags (-O, -A etc.)
     * @param IP ip to be scanned
     * @param periodic whether this is a periodic job (true/false in String)
     * @param time period of periodic job execution
     * @param position position of selected SA in the spinner or list
     */
    public Jobs(String parameters, String IP, String periodic, String time, String position){
        this.setParameters(parameters);
        this.setIP(IP);
        this.setPeriodic(periodic);
        this.setTime(time);
        this.setPosition(position);
    }


    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPeriodic() {
        return periodic;
    }

    public void setPeriodic(String periodic) {
        this.periodic = periodic;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
