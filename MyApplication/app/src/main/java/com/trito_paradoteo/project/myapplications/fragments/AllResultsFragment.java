package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.NumberPicker;
import android.widget.TextView;
import org.json.JSONArray;
import java.util.ArrayList;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.reguest.server.RequestDataFromServer;



public class AllResultsFragment extends Fragment {

    private NumberPicker numberPicker;
    private int numberOfRes;
    private StringBuilder stringResultsBuffer = new StringBuilder();
    private TextView text;

    public AllResultsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_all_res, container, false);

        text = (TextView) rootView.findViewById(R.id.textAllRes);
        text.setText(stringResultsBuffer.toString());

        numberPicker = (NumberPicker) rootView.findViewById(R.id.numberPickerForAllRes);
        numberPicker.setMaxValue(20);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setValue(1);
        loadAllResults(rootView, 1);

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                numberOfRes = picker.getValue();
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setMessage("Number of results selected :"+Integer.toString(numberOfRes));
                dialogBuilder.setPositiveButton("Ok", null);
                dialogBuilder.show();

                loadAllResults(rootView, numberOfRes);
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private StringBuilder loadAllResults(final View view, int numberOfRes){
        RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.fetchAllResultsInBackground(numberOfRes, new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {}

            @Override
            public void done(StringBuilder stringBuilder) {
                if (stringBuilder == null) {
                    errorMessageDialog();
                }
                //requestDataFromServer.setSb(stringBuilder);
                stringResultsBuffer = stringBuilder;
                text = (TextView) view.findViewById(R.id.textAllRes);
                text.setText(stringResultsBuffer.toString());
                //Toast.makeText(getContext(), (String) stringResultsBuffer.toString() + " inside onPost".toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void done() {}

            @Override
            public void done(JSONArray jsonArray) {}
        });

        return stringResultsBuffer;
    }

    /**
     * print error in failure
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Something went wrong. Retry!");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

}
