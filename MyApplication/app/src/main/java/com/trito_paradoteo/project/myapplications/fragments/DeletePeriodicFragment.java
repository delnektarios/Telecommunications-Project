package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import org.json.JSONArray;
import java.util.ArrayList;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.reguest.server.RequestDataFromServer;

public class DeletePeriodicFragment extends Fragment {

    private ListView listView;
    private Button terminate;
    private int periodicPosition = -1;
    private ArrayList<String> arrayListOfPeriodics = new ArrayList<String>();

    public DeletePeriodicFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_delete_periodic, container, false);

        listView = (ListView) rootView.findViewById(R.id.listOfPeriodics);
        terminate = (Button) rootView.findViewById(R.id.terminatePeriodicButton);

        loadListOfPeriodics();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                periodicPosition = position;
            }
        });

        terminate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(periodicPosition == -1){
                    return;
                }
                terminatePeriodic(periodicPosition);
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * print error
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("There are no periodic jobs!");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

    /**
     * loading a list of the periodic jobs from server
     */
    private void loadListOfPeriodics(){
        final RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.fetchPeriodicsInBackground(new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {
                if (arrayList.size() == 0) {
                    errorMessageDialog();
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, arrayList);
                listView.setAdapter(adapter);
            }

            @Override
            public void done(StringBuilder stringBuilder) {}

            @Override
            public void done() {}

            @Override
            public void done(JSONArray jsonArray) {}
        });
        return;
    }

    /**
     * termiate a selected periodic using its position on the list
     * @param position
     */
    private void terminatePeriodic(int position){
        final RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.terminatePeriodicInBackground(position, new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {}

            @Override
            public void done(StringBuilder stringBuilder) {}

            @Override
            public void done() {
                loadListOfPeriodics();
            }

            @Override
            public void done(JSONArray jsonArray) {}
        });

        return;
    }

}
