package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.reguest.server.RequestDataFromServer;



/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EnteringNmapJobsFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EnteringNmapJobsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EnteringNmapJobsFragment extends Fragment {

    private Spinner spinnerFlags, spinnerIP, spinnerPeriodic, spinnerJobs, SAspinner;
    private Button commitJobs;
    private EditText editFlags, editIP, editPeriodic, editTime;
    private int counter = 0;
    private int SAposition = 0;

    public EnteringNmapJobsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_nmap_jobs, container, false);

        spinnerFlags = (Spinner) rootView.findViewById(R.id.flagSpinner);
        spinnerIP = (Spinner) rootView.findViewById(R.id.IPSpinner);
        spinnerPeriodic = (Spinner) rootView.findViewById(R.id.periodicSpinner);
        SAspinner = (Spinner) rootView.findViewById(R.id.SAspinner);

        spinnerJobs = (Spinner) rootView.findViewById(R.id.existingJobsSpinner);
        //updating spinner with jobs from server
        loadAllJobs();

        editFlags = (EditText) rootView.findViewById(R.id.editFlags);
        editIP = (EditText) rootView.findViewById(R.id.editIP);
        editPeriodic = (EditText) rootView.findViewById(R.id.editPeriodic);
        editTime = (EditText) rootView.findViewById(R.id.editTime);

        editTime.setText("1");
        editIP.setText("localhost");
        editPeriodic.setText("false");

        loadListOfSAs();

        spinnerFlags.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = editFlags.getText().toString();
                String flag_info = parent.getSelectedItem().toString();
                String[] flag = flag_info.split(":");
                editFlags.setText(text+" "+flag[0]+" ");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerIP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                editIP.setText(parent.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerPeriodic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                editPeriodic.setText(parent.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        SAspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SAposition = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerJobs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String str = spinnerJobs.getItemAtPosition(position).toString();
                //Toast.makeText(parent.getContext(), str, Toast.LENGTH_SHORT).show();
                String args[] = str.split(",");
                editTime.setText(args[2]);
                //editIP.setText(args[]);
                editPeriodic.setText(args[1]);
                String param[] = args[0].split(" ");
                int l = param.length;
                editIP.setText(param[l-1]);
                String flg = "";
                for(int i=0;i<l-1;i++){
                    flg = flg+param[i]+" ";
                }
                flg.replace("-oX -","");
                editFlags.setText(flg);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        commitJobs = (Button) rootView.findViewById(R.id.commitbutton);
        commitJobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //EditText editFlags, editIP, editPeriodic, editTime;
                counter++;
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", Integer.toString(counter));
                    String parameters = editFlags.getText().toString()+" -oX - "+editIP.getText().toString();

                    Toast.makeText(getContext(), parameters, Toast.LENGTH_SHORT).show();

                    //create a class to hold the periodic jobs

                    jsonObject.put("parameters", parameters);
                    jsonObject.put("periodic", editPeriodic.getText().toString());
                    jsonObject.put("time", editTime.getText().toString());
                    jsonObject.put("position", Integer.toString(SAposition));

                    RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
                    requestDataFromServer.sendJobToSAInBackground(jsonObject, SAposition);

                    loadListOfSAs();

                } catch (Exception e) {
                    e.printStackTrace();
                    errorMessageDialog();
                }
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * loading a list of the SAs
     */
    private void loadListOfSAs(){
        final RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.fetchSAsInBackground(new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {
                if (arrayList.size() == 0) {
                    //errorMessageDialog();
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, arrayList);
                SAspinner.setAdapter(adapter);
            }
            @Override
            public void done(StringBuilder stringBuilder) {}

            @Override
            public void done() {}

            @Override
            public void done(JSONArray jsonArray) {}
        });
        //Toast.makeText(getContext(),(String)requestDataFromServer.getArrayList().toString()+" getting from method".toString(),Toast.LENGTH_SHORT).show();
        return ;
    }

    /**
     * loading previous given jobs from server
     */
    private void loadAllJobs(){
        RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.fetchAllJobsInBackground(new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, arrayList);
                spinnerJobs.setAdapter(adapter);
            }

            @Override
            public void done(StringBuilder stringBuilder) {

            }

            @Override
            public void done(JSONArray jsonArray) {

            }

            @Override
            public void done() {

            }
        });
    }

    /**
     * prints error message in failure
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Something went wrong. Retry!");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }
}
