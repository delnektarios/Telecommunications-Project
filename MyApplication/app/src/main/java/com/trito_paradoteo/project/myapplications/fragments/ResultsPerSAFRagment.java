package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.NumberPicker;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.reguest.server.RequestDataFromServer;

import org.json.JSONArray;

import java.util.ArrayList;

public class ResultsPerSAFRagment extends Fragment {

    private ListView listView;
    private NumberPicker numberPicker;
    private int numberOfRes;
    private ArrayList<String> arrayListOfSAs = new ArrayList<String>();
    public static final int CONNECTION_TIMEOUT_TIME = 1000 * 15;

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;


    public ResultsPerSAFRagment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_results_per_sa, container, false);

        numberPicker = (NumberPicker) rootView.findViewById(R.id.numberPicker);
        numberPicker.setMaxValue(10);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);
        numberPicker.setValue(1);

        numberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                numberOfRes = picker.getValue();
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
                dialogBuilder.setMessage("Number of results selected :"+Integer.toString(numberOfRes));
                dialogBuilder.setPositiveButton("Ok", null);
                dialogBuilder.show();
            }
        });

        listView = (ListView) rootView.findViewById(R.id.listOfSAs);

        arrayListOfSAs = loadListOfSAs();
        //Toast.makeText(getContext(),(String)arrayListOfSAs.toString()+" inside list".toString(),Toast.LENGTH_SHORT).show();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ShowResultsPerSA showResultsPerSA = new ShowResultsPerSA();
                Bundle bundle = new Bundle();
                bundle.putInt("position", position );
                bundle.putInt("numberOfResults", numberOfRes);
                showResultsPerSA.setArguments(bundle);
                //fragmentManager = getFragmentManager().executePendingTransactions();
                fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentholder, showResultsPerSA);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        setDefaultListSelection();

        // Inflate the layout for this fragment
        return rootView;
    }

    private void setDefaultListSelection(){
        listView.setItemChecked(0, true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * prints error message in failure
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Something went wrong. Retry!");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

    /**
     * loading a list of the SAs from server
     * @return list of SAs in ArrayList<String>
     */
    private ArrayList<String> loadListOfSAs(){
        final RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.fetchSAsInBackground(new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {
                if (arrayList.size() == 0) {
                    errorMessageDialog();
                }
                //Toast.makeText(getContext(),(String)arrayList.toString()+" inside onPost".toString(),Toast.LENGTH_SHORT).show();
                requestDataFromServer.setArrayListSA(arrayList);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, arrayList);
                listView.setAdapter(adapter);

            }
            @Override
            public void done(StringBuilder stringBuilder) {}

            @Override
            public void done() {}

            @Override
            public void done(JSONArray jsonArray) {}
        });
        return requestDataFromServer.getArrayList();
    }
}
