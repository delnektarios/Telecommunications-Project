package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.reguest.server.RequestDataFromServer;

import org.json.JSONArray;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link SATerminationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link SATerminationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SATerminationFragment extends Fragment {

    private ArrayList<String> arrayListOfSAs = new ArrayList<String>();

    private ListView listView;
    private Button terminateButton;
    private int SAposition;
    private int isClicked = 0;


    public SATerminationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_satermination, container, false);

        listView = (ListView) rootView.findViewById(R.id.listOfSAsTerm);
        terminateButton = (Button) rootView.findViewById(R.id.terminatebutton);

        loadListOfSAs();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                isClicked = 1;
                SAposition = position;
            }
        });

        terminateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isClicked == 1){
                    terminateSA();
                }else{
                    errorMessageDialog();
                }
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * prints error message in case of failure
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Something went wrong. Retry!");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

    /**
     * getting a list of the SAs from server
     * @return an arraylist of the SAs
     */
    private ArrayList<String> loadListOfSAs(){
        final RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.fetchSAsInBackground(new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {
                if (arrayList.size() == 0) {
                    //errorMessageDialog();
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, arrayList);
                listView.setAdapter(adapter);

            }

            @Override
            public void done(StringBuilder stringBuilder) {}

            @Override
            public void done() {}

            @Override
            public void done(JSONArray jsonArray) {}
        });
        //Toast.makeText(getContext(),(String)requestDataFromServer.getArrayList().toString()+" getting from method".toString(),Toast.LENGTH_SHORT).show();

        return requestDataFromServer.getArrayList();
    }

    /**
     * preparing SAtermination and refreshing the list
     */
    private void terminateSA(){
        RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.terminateSAInBackground(SAposition, new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {}

            @Override
            public void done(StringBuilder stringBuilder) {}

            @Override
            public void done(JSONArray jsonArray) {}

            @Override
            public void done() {
                isClicked = 0;
                loadListOfSAs();
            }
        });
        return ;
    }
}
