package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.reguest.server.RequestDataFromServer;

import org.json.JSONArray;

import java.util.ArrayList;

public class SA_status_fragment extends Fragment {

    ListView listView;
    Button refresh;

    public SA_status_fragment() {
        // Required empty public constructor
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.sa_status_fragment, container, false);

        refresh = (Button) rootView.findViewById(R.id.refresh);
        listView = (ListView) rootView.findViewById(R.id.listStatus);

        loadStatus();

        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadStatus();
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

//    private void errorMessageDialog(){
//        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
//        dialogBuilder.setMessage("Something went wrong. Retry!");
//        dialogBuilder.setPositiveButton("Ok", null);
//        dialogBuilder.show();
//    }

    /**
     * getting SAs status from server
     */
    private void loadStatus(){
        final RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.getSAStatusInBackground(new GetServerCallBack() {

            @Override
            public void done(ArrayList<String> arrayList) {
                if (arrayList.size() == 0) {
                    //errorMessageDialog();
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_activated_1, arrayList);
                listView.setAdapter(adapter);

            }

            @Override
            public void done(StringBuilder stringBuilder) {
            }

            @Override
            public void done() {
            }

            @Override
            public void done(JSONArray jsonArray) {}
        });

        return;
    }


}
