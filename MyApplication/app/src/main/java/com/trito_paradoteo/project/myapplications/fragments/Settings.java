package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.trito_paradoteo.project.myapplications.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends Fragment {


    private EditText ip;
    private Button button;
    public static final String DEFAULT_IP = "10.0.2.2";
    //private final String DEFAULT_IP = "localhost";
    //public static String com_ip = null;  /CORRECT VERSION
    //public static String com_ip = "localhost";
    public static String com_ip = "10.0.2.2";

    public Settings() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        ip = (EditText) rootView.findViewById(R.id.ip);
        button = (Button) rootView.findViewById(R.id.confirm);

        String IP = ip.getText().toString();
        Toast.makeText(getContext(), IP, Toast.LENGTH_SHORT).show();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String IP = ip.getText().toString();
                com_ip = IP;
                Toast.makeText(getContext(), com_ip, Toast.LENGTH_SHORT).show();
            }
        });

        return rootView;
    }
}
