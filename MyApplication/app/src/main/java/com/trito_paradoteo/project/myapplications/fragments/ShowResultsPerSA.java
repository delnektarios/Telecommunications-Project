package com.trito_paradoteo.project.myapplications.fragments;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.R;
import com.trito_paradoteo.project.myapplications.reguest.server.RequestDataFromServer;

import org.json.JSONArray;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ShowResultsPerSA extends Fragment {

    private int position = 0;
    private int numberOfResults = 0;
    private StringBuilder stringResultsBuffer = new StringBuilder();
    private TextView text;

    public ShowResultsPerSA() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_show_results_per_sa, container, false);

        Bundle mBundle = new Bundle();
        mBundle = getArguments();
        if(mBundle!=null)
        {
            position = mBundle.getInt("position");
            numberOfResults = mBundle.getInt("numberOfResults");
            //Toast.makeText(getContext(), Integer.toString(position)+" position", Toast.LENGTH_SHORT).show();
            //Toast.makeText(getContext(), Integer.toString(numberOfResults)+" number", Toast.LENGTH_SHORT).show();
        }

        loadAllResults(rootView);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
            }
        }, 500);

        //Toast.makeText(getContext(), stringResultsBuffer.toString()+" inside fragment", Toast.LENGTH_SHORT).show();

        text = (TextView) rootView.findViewById(R.id.results);
        text.setText(stringResultsBuffer.toString());


        return rootView;
    }

    /**
     * loading all results from server
     * @param view
     * @return
     */
    private StringBuilder loadAllResults(final View view){
        RequestDataFromServer requestDataFromServer = new RequestDataFromServer(getContext());
        requestDataFromServer.fetchSAResultsInBackground(numberOfResults, position, new GetServerCallBack() {
            @Override
            public void done(ArrayList<String> arrayList) {
            }

            @Override
            public void done(StringBuilder stringBuilder) {
                if (stringBuilder == null) {
                    errorMessageDialog();
                }
                //requestDataFromServer.setSb(stringBuilder);
                stringResultsBuffer = stringBuilder;
                text = (TextView) view.findViewById(R.id.results);
                text.setText(stringResultsBuffer.toString());
                //Toast.makeText(getContext(), (String) stringResultsBuffer.toString() + " inside onPost".toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void done() {}

            @Override
            public void done(JSONArray jsonArray) {}
        });

        return stringResultsBuffer;
    }

    /**
     * prints error message in failure
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setMessage("Something went wrong. Retry!");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }

}
