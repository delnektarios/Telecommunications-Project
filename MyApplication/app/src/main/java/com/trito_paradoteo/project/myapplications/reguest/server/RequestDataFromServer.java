package com.trito_paradoteo.project.myapplications.reguest.server;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */

import android.app.ProgressDialog;
import android.content.Context;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.fetchAllJobsAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.fetchAllResultsAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.fetchPeriodicsAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.fetchSAResultsAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.fetchSAsAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.getSAStatusAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.sendJobToSaAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.terminatePeriodicAsyncTask;
import com.trito_paradoteo.project.myapplications.reguest.server.async_tasks.terminateSaAsyncTask;

import org.json.JSONObject;

import java.util.ArrayList;

public class RequestDataFromServer {

    private ProgressDialog progressDialog;
    private Context context;
    public static final int CONNECTION_TIMEOUT_TIME = 15 * 1000;

    private ArrayList<String> arrayListSA = new ArrayList<String>();

    /**
     * constructor for RequestDataFromServer
     * @param context
     */
    public RequestDataFromServer(Context context){
        this.context = context;
        setProgressDialog(new ProgressDialog(context));
        getProgressDialog().setCancelable(false);
        getProgressDialog().setTitle("Processing Request to Server");
        getProgressDialog().setMessage("Please wait.......");
    }

    /**
     * to fetch SAs in the background
     * @param serverCallBackArrayList interface to collect the results
     */
    public void fetchSAsInBackground(GetServerCallBack serverCallBackArrayList){
        getProgressDialog().show();
        new fetchSAsAsyncTask(serverCallBackArrayList, progressDialog).execute();
    }

    /**
     * fetching results per SA
     * @param numberOfResults number of the latest results to show from the numberpicker
     * @param position position of the desired SA in the list shown
     * @param serverCallBackStringBuffer interface to collect the results
     */
    public void fetchSAResultsInBackground(int numberOfResults,int position, GetServerCallBack serverCallBackStringBuffer){
        getProgressDialog().show();
        new fetchSAResultsAsyncTask(numberOfResults, position, serverCallBackStringBuffer, progressDialog).execute();

    }

    /**
     * fetching all the latest results
     * @param numberOfResults desired number to show
     * @param serverCallBackStringBuffer interface to collect the results
     */
    public void fetchAllResultsInBackground(int numberOfResults, GetServerCallBack serverCallBackStringBuffer){
        getProgressDialog().show();
        new fetchAllResultsAsyncTask(numberOfResults, serverCallBackStringBuffer, progressDialog).execute();

    }

    /**
     * sending a job to a SA
     * @param jsonObject which contains the job specifications
     * @param position position of the SA IN THE SPINNER SHOWN
     */
    public void sendJobToSAInBackground(JSONObject jsonObject, int position){
        getProgressDialog().show();
        new sendJobToSaAsyncTask(jsonObject, position, progressDialog, context).execute();
    }

    /**
     * fetching all jobs from the past
     * @param serverCallBack interface to collect results
     */
    public void fetchAllJobsInBackground(GetServerCallBack serverCallBack){
        getProgressDialog().show();
        new fetchAllJobsAsyncTask(serverCallBack, progressDialog).execute();
    }

    /**
     * terminate a SA
     * @param position position of SA in the list
     * @param serverCallBackArrayList interface to collect results
     */
    public void terminateSAInBackground(int position, GetServerCallBack serverCallBackArrayList){
        getProgressDialog().show();
        new terminateSaAsyncTask(position, serverCallBackArrayList, progressDialog, context).execute();

    }

    /**
     * fetch all periodic jobs
     * @param serverCallBackArrayList
     */
    public void fetchPeriodicsInBackground(GetServerCallBack serverCallBackArrayList){
        getProgressDialog().show();
        new fetchPeriodicsAsyncTask(serverCallBackArrayList, progressDialog).execute();
    }


    /**
     * terminate a SA
     * @param position
     * @param serverCallBack
     */
    public void terminatePeriodicInBackground(int position, GetServerCallBack serverCallBack){
        getProgressDialog().show();
        new terminatePeriodicAsyncTask(position, serverCallBack, progressDialog, context).execute();
    }

    /**
     * get status of SAs
     * @param serverCallBack
     */
    public void getSAStatusInBackground(GetServerCallBack serverCallBack){
        getProgressDialog().show();
        new getSAStatusAsyncTask(serverCallBack, progressDialog).execute();
    }

    public ArrayList<String> getArrayList() {
        //Toast.makeText(context,(String)arrayListSA.toString(),Toast.LENGTH_SHORT).show();
        return arrayListSA;
    }

    public void setArrayListSA(ArrayList<String> arrayListSA) {
        this.arrayListSA = arrayListSA;
        //Toast.makeText(context, arrayListSA.toString()+" in setter", Toast.LENGTH_LONG).show();
    }


    public ProgressDialog getProgressDialog() {
        return progressDialog;
    }

    public void setProgressDialog(ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
    }

}
