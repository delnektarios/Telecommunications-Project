package com.trito_paradoteo.project.myapplications.reguest.server;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.trito_paradoteo.project.myapplications.GetUserCallBack;
import com.trito_paradoteo.project.myapplications.users.User;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by nektarios on 1/16/16.
 */
public class ServerRequests {

    private ProgressDialog progressDialog;
    public static final int CONNECTION_TIMEOUT_TIME = 1000 * 15;
    //public static final String SERVER_ADDR = ""; //MUST FILL IN!!!!!!

    public ServerRequests(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("Processing Request to Server");
        progressDialog.setMessage("Please wait.......");
    }

    //used when resgistering a new user
    public void storeUserInBackground(User user, GetUserCallBack userCallBack){
        progressDialog.show();
        new storeUserAsyncTask(user, userCallBack).execute();
    }

    //
    public void fetchUserInBackground(User user, GetUserCallBack userCallBack){
        progressDialog.show();
        new fetchUserAsyncTask(user, userCallBack).execute();

    }


    public class storeUserAsyncTask extends AsyncTask<Void, Void, Void>{

        User user;
        GetUserCallBack userCallBack;

        public storeUserAsyncTask(User user, GetUserCallBack userCallBack){
            this.user = user;
            this.userCallBack =userCallBack;
        }

        @Override
        protected Void doInBackground(Void... params) {

            JSONObject obj = new JSONObject();
            try {
                obj.put("name",user.getName());
                obj.put("service",user.getService());
                obj.put("username",user.getUsername());
                obj.put("password",user.getPassword());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //hitting webservices to store a new user
            // 10.0.2.2 for emulator
            // localhost for real device
            HttpURLConnection urlConnection = null;
            try{
                //http://stackoverflow.com/questions/13911993/sending-a-json-http-post-request-from-android
                System.setProperty("http.keepAlive", "false");
                URL urlToRequest = new URL("http://10.0.2.2:8080/android/storeUser");
                //URL urlToRequest = new URL("http://localhost:8080/android/storeUser");
                DataOutputStream printout;
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                //urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
                urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
                urlConnection.setRequestMethod("PUT");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("connection", "close");
                urlConnection.connect();

                String str = obj.toString();
                byte[] data = str.getBytes("UTF-8");

                // Send POST output.
                printout = new DataOutputStream(urlConnection.getOutputStream());
                printout.write(data);
                printout.flush();
                printout.close();

                int statusCode = urlConnection.getResponseCode();

                if (statusCode == HttpURLConnection.HTTP_OK) {
                    //everything went ok
                }else if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    // handle unauthorized (if service requires user login)
                } else if (statusCode != HttpURLConnection.HTTP_OK) {
                    // handle any other errors, like 404, 500,..
                }

            }catch(IOException e){
                e.printStackTrace();
            }
            finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            userCallBack.done(null);
            super.onPostExecute(aVoid);
        }
    }


    public class fetchUserAsyncTask extends AsyncTask<Void, Void, User>{

        User user;
        GetUserCallBack userCallBack;

        public fetchUserAsyncTask(User user, GetUserCallBack userCallBack){
            this.user = user;
            this.userCallBack = userCallBack;
        }

        @Override
        protected User doInBackground(Void... params) {
            //hitting webservices to fetch an existing user
            // 10.0.2.2 for emulator
            // localhost for real device

            User returnedUser = null;
            HttpURLConnection urlConnection = null;
            try{
                System.setProperty("http.keepAlive", "false");
                URL urlToRequest = new URL("http://10.0.2.2:8080/android/fetchUser/"+user.getUsername()+"/"+user.getPassword());
                //URL urlToRequest = new URL("http://localhost:8080/android/fetchUser/"+user.getUsername()+"/"+user.getPassword());
                urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
                urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
                urlConnection.setRequestMethod("GET");
                urlConnection.setUseCaches(false);
                urlConnection.setRequestProperty("connection", "close");
                urlConnection.connect();

                int statusCode = urlConnection.getResponseCode();

                if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    // handle unauthorized (if service requires user login)
                } else if (statusCode != HttpURLConnection.HTTP_OK) {
                    // handle any other errors, like 404, 500,..
                }

                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String res = getResponseText(in);

                JSONObject obj = new JSONObject(res);
                String name = obj.getString("name");
                String service = obj.getString("service");
                Boolean registered = obj.getBoolean("registered");

                if (registered){
                    returnedUser = new User(name,service,user.getUsername(),user.getPassword());
                }

            }catch(Exception e){
                e.printStackTrace();
            }finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }


            return returnedUser;
        }

        @Override
        protected void onPostExecute(User returnedUser) {
            progressDialog.dismiss();
            userCallBack.done(returnedUser);
            super.onPostExecute(user);
        }

    }

    /**
     * getting the response from restfull web services from server
     * @param inStream input stream
     * @return the inStream in String format
     */
    private static String getResponseText(InputStream inStream) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
