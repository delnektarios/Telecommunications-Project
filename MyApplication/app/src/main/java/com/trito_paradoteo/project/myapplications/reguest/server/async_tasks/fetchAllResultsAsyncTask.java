package com.trito_paradoteo.project.myapplications.reguest.server.async_tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.fragments.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public class fetchAllResultsAsyncTask extends AsyncTask<Void, Void, StringBuilder> {

    private ProgressDialog progressDialog;
    private StringBuilder sb = new StringBuilder();
    private String res;
    public static final int CONNECTION_TIMEOUT_TIME = 15 * 1000;

    private ArrayList<String> arrayList = new ArrayList<String>();
    private int numberOfResults;
    private int position;
    private GetServerCallBack serverCallBack;

    public fetchAllResultsAsyncTask(int numberOfResults, GetServerCallBack serverCallBack, ProgressDialog progressDialog){
        this.position = position;
        this.numberOfResults = numberOfResults;
        //Toast.makeText(context, Integer.toString(numberOfResults)+" number of res", Toast.LENGTH_SHORT).show();
        this.serverCallBack = serverCallBack;
        this.progressDialog = progressDialog;
    }

    @Override
    protected StringBuilder doInBackground(Void... params) {

        //updating list of SAs from server

        HttpURLConnection urlConnection = null;
        URL urlToRequest = null;
        try{
            System.setProperty("http.keepAlive", "false");
            if (Settings.com_ip == null){
                //urlToRequest = new URL("http://10.0.2.2:8080/android/fetchAllResults/"+Integer.toString(numberOfResults));
                urlToRequest = new URL("http://localhost:8080/android/fetchAllResults/"+Integer.toString(numberOfResults));
            }else{
                urlToRequest = new URL("http://"+Settings.com_ip+":8080/android/fetchAllResults/"+Integer.toString(numberOfResults));
            }

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setRequestMethod("GET");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("connection", "close");
            urlConnection.connect();

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            res = getResponseText(in);

            JSONArray results = new JSONArray(res);

            for(int i=0;i<results.length();i++) {
                JSONObject obj = results.getJSONObject(i);
                sb.append(obj.get("scanner") + "\n");
                sb.append(obj.get("command arguments") + "\n");
                sb.append(obj.get("start time") + "\n");
                sb.append(obj.get("protocol") + "\n");
                sb.append(obj.get("summary") + "\n");
                sb.append("----------\n");
            }

        }catch(Exception e){
            e.printStackTrace();
        }finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return sb;
    }

    @Override
    protected void onPostExecute(StringBuilder stringBuilder) {
        progressDialog.dismiss();
        serverCallBack.done(stringBuilder);
        super.onPostExecute(stringBuilder);
    }

    /**
     * getting the response in string format
     * @param inStream
     * @return
     */
    private static String getResponseText(InputStream inStream) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
