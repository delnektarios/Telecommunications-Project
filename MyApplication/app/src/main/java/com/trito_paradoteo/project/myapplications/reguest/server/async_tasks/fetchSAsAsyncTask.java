package com.trito_paradoteo.project.myapplications.reguest.server.async_tasks;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.fragments.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by nektarios on 2/22/16.
 */
public class fetchSAsAsyncTask extends AsyncTask<Void, Void, ArrayList<String> > {

    public static final int CONNECTION_TIMEOUT_TIME = 15 * 1000;
    //ArrayList<String> arrayList = new ArrayList<String>();
    GetServerCallBack serverCallBack;
    private ProgressDialog progressDialog;
    private ArrayList<String> arrayListSA = new ArrayList<String>();
    private String res;

    public fetchSAsAsyncTask(GetServerCallBack serverCallBack, ProgressDialog progressDialog){
        this.serverCallBack = serverCallBack;
        this.progressDialog = progressDialog;
    }

    @Override
    protected ArrayList<String> doInBackground(Void... params) {

        HttpURLConnection urlConnection = null;
        URL urlToRequest = null;
        try{
            System.setProperty("http.keepAlive", "false");
            if (Settings.com_ip == null){
                //urlToRequest = new URL("http://10.0.2.2:8080/android/fetchSAs");
                urlToRequest = new URL("http://localhost:8080/android/fetchSAs");
            }else{
                urlToRequest = new URL("http://"+Settings.com_ip+":8080/android/fetchSAs");
            }
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setRequestMethod("GET");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("connection", "close");
            urlConnection.connect();

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..
            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            res = getResponseText(in);

            JSONArray array = new JSONArray(res);

            for(int i=0;i<array.length();i++){
                JSONObject obj = array.getJSONObject(i);
                arrayListSA.add((String) obj.get("SA"));
            }

        }catch(Exception e){

        }finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return arrayListSA;
    }

    @Override
    protected void onPostExecute(ArrayList<String> strings) {
        progressDialog.dismiss();
        serverCallBack.done(strings);
    }

    /**
     * getting response
     * @param inStream
     * @return
     */
    private static String getResponseText(InputStream inStream) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
