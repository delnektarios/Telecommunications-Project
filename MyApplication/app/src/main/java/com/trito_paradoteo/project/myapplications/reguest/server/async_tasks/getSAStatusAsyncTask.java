package com.trito_paradoteo.project.myapplications.reguest.server.async_tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.fragments.Settings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public class getSAStatusAsyncTask extends AsyncTask<Void, Void, ArrayList<String> > {

    private ProgressDialog progressDialog;
    private Context context;
    public static final int CONNECTION_TIMEOUT_TIME = 15 * 1000;
    GetServerCallBack serverCallBack;

    private ArrayList<String > arrayListStatus = new ArrayList<String>();
    private String res;

    public getSAStatusAsyncTask(GetServerCallBack serverCallBack, ProgressDialog progressDialog){
        this.serverCallBack = serverCallBack;
        this.progressDialog = progressDialog;
    }

    @Override
    protected ArrayList<String> doInBackground(Void... params) {

        HttpURLConnection urlConnection = null;
        URL urlToRequest = null;
        try{
            System.setProperty("http.keepAlive", "false");
            if (Settings.com_ip == null){
                urlToRequest = new URL("http://10.0.2.2:8080/android/fetchSAsStatus");
                //urlToRequest = new URL("http://localhost:8080/android/fetchSAsStatus");
            }else{
                urlToRequest = new URL("http://"+Settings.com_ip+":8080/android/fetchSAsStatus");
            }

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setRequestMethod("GET");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("connection", "close");
            urlConnection.connect();

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..

            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            res = getResponseText(in);

            JSONArray array = new JSONArray(res);

            for(int i=0;i<array.length();i++){
                JSONObject obj = array.getJSONObject(i);
                arrayListStatus.add((String) obj.get("status"));
            }

        }catch(Exception e){

        }finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return arrayListStatus;
    }

    @Override
    protected void onPostExecute(ArrayList<String> strings) {
        progressDialog.dismiss();
        serverCallBack.done(strings);
    }

    /**
     * getting the response
     * @param inStream
     * @return
     */
    private static String getResponseText(InputStream inStream) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }
}
