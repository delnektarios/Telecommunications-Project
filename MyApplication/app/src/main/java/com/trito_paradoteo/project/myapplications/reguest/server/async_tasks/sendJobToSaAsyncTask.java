package com.trito_paradoteo.project.myapplications.reguest.server.async_tasks;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.trito_paradoteo.project.myapplications.activities.MainActivity;
import com.trito_paradoteo.project.myapplications.database.sqlite.DatabaseHandler;
import com.trito_paradoteo.project.myapplications.database.sqlite.Jobs;
import com.trito_paradoteo.project.myapplications.fragments.Settings;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public class sendJobToSaAsyncTask extends AsyncTask<Void, Void, Void > {

    private DatabaseHandler db = MainActivity.databaseHandler;
    private List<Integer> index = MainActivity.index;

    private ProgressDialog progressDialog;
    private Context context;
    private String res;
    public static final int CONNECTION_TIMEOUT_TIME = 15 * 1000;

    private JSONObject jsonObject;
    private int position;
    private int exception = 0;
    private boolean sent = true;

    public sendJobToSaAsyncTask(JSONObject jsonObject, int position, ProgressDialog progressDialog, Context context){
        this.jsonObject = jsonObject;
        this.position = position;
        this.progressDialog = progressDialog;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {

        //Toast.makeText(context, jsonObject.toString()+">"+Integer.toString(position), Toast.LENGTH_SHORT).show();
        HttpURLConnection urlConnection = null;
        URL urlToRequest = null;
        try{
            //http://stackoverflow.com/questions/13911993/sending-a-json-http-post-request-from-android
            System.setProperty("http.keepAlive", "false");
            if (Settings.com_ip == null){
                //urlToRequest = new URL("http://10.0.2.2:8080/android/AcceptJob");
                urlToRequest = new URL("http://localhost:8080/android/AcceptJob");
            }else{
                urlToRequest = new URL("http://"+Settings.com_ip+":8080/android/AcceptJob");
            }

            DataOutputStream printout;
            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            //urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setRequestMethod("PUT");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("connection", "close");
            urlConnection.connect();

            String str = jsonObject.toString();
            byte[] data = str.getBytes("UTF-8");

            // Send POST output.
            printout = new DataOutputStream(urlConnection.getOutputStream());
            printout.write(data);
            printout.flush();
            printout.close();

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_OK) {
                //everything went ok
            }else if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..
                exception  = 1;
                sent = false;
            }

        }catch(IOException e){
            //errorMessageDialog();
            exception = 1;
            sent = false;
            e.printStackTrace();

        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        //if you sent a job successfuly check if database has jobs to be sent that previously failed to be sent
        if (sent){
            List<Jobs> list = db.getAllJobs();
            for (Jobs job : list){
                JSONObject obj = new JSONObject();
                try {
                    obj.put("parameters", job.getParameters());
                    obj.put("periodic", job.getPeriodic());
                    obj.put("time", job.getTime());
                    obj.put("position", job.getPosition());

                    urlConnection = null;
                    urlToRequest = null;
                    try{
                        //http://stackoverflow.com/questions/13911993/sending-a-json-http-post-request-from-android
                        System.setProperty("http.keepAlive", "false");
                        if (Settings.com_ip == null){
                            //urlToRequest = new URL("http://10.0.2.2:8080/android/AcceptJob");
                            urlToRequest = new URL("http://localhost:8080/android/AcceptJob");
                        }else{
                            urlToRequest = new URL("http://"+Settings.com_ip+":8080/android/AcceptJob");
                        }
                        DataOutputStream printout;
                        urlConnection = (HttpURLConnection) urlToRequest.openConnection();
                        //urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
                        urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
                        urlConnection.setRequestMethod("PUT");
                        urlConnection.setRequestProperty("Content-Type", "application/json");
                        urlConnection.setDoInput(true);
                        urlConnection.setDoOutput(true);
                        urlConnection.setUseCaches(false);
                        urlConnection.setRequestProperty("connection", "close");
                        urlConnection.connect();

                        String str = obj.toString();
                        byte[] data = str.getBytes("UTF-8");

                        // Send POST output.
                        printout = new DataOutputStream(urlConnection.getOutputStream());
                        printout.write(data);
                        printout.flush();
                        printout.close();

                        int statusCode = urlConnection.getResponseCode();

                        if (statusCode == HttpURLConnection.HTTP_OK) {
                            //everything went ok
                        }else if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                            // handle unauthorized (if service requires user login)
                        } else if (statusCode != HttpURLConnection.HTTP_OK) {
                            // handle any other errors, like 404, 500,..
                        }

                    }catch(IOException e){
                        e.printStackTrace();
                    }
                    finally {
                        if (urlConnection != null) {
                            urlConnection.disconnect();
                        }
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            //db.deleteRecords();
            db.deleteAllRecords();
            //db.reset(); //resetting database
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progressDialog.dismiss();
        super.onPostExecute(aVoid);
        if (exception == 1){
            errorMessageDialog();
            Jobs job = new Jobs();
            try {
                job.setParameters((String) jsonObject.get("parameters"));
                job.setPeriodic((String) jsonObject.get("periodic"));
                job.setTime((String) jsonObject.get("time"));
                job.setPosition((String) jsonObject.get("position"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            db.addJob(job);
        }
    }

    /**
     *
     * @param inStream
     * @return
     */
    private static String getResponseText(InputStream inStream) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setMessage("Error occured while trying to connect!\n");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }
}
