package com.trito_paradoteo.project.myapplications.reguest.server.async_tasks;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.trito_paradoteo.project.myapplications.GetServerCallBack;
import com.trito_paradoteo.project.myapplications.activities.MainActivity;
import com.trito_paradoteo.project.myapplications.database.sqlite.DatabaseHandler;
import com.trito_paradoteo.project.myapplications.database.sqlite.Jobs;
import com.trito_paradoteo.project.myapplications.fragments.Settings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public class terminateSaAsyncTask extends AsyncTask<Void, Void, Void> {

    private DatabaseHandler db = MainActivity.databaseHandler;

    private ProgressDialog progressDialog;
    private Context context;
    private String res;
    public static final int CONNECTION_TIMEOUT_TIME = 15 * 1000;

    private ArrayList<String> arrayListSA = new ArrayList<String>();
    private StringBuilder sb = new StringBuilder();

    private int position;
    private int exception = 0;
    private GetServerCallBack serverCallBack;

    public terminateSaAsyncTask(int position, GetServerCallBack serverCallBack, ProgressDialog progressDialog, Context context){
        this.position = position;
        this.serverCallBack= serverCallBack;
        this.progressDialog = progressDialog;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... params) {

        HttpURLConnection urlConnection = null;
        URL urlToRequest = null;
        try{
            System.setProperty("http.keepAlive", "false");
            if (Settings.com_ip == null){
                //urlToRequest = new URL("http://10.0.2.2:8080/android/TerminateSA/"+Integer.toString(position));
                urlToRequest = new URL("http://localhost:8080/android/TerminateSA/"+Integer.toString(position));
            }else{
                urlToRequest = new URL("http://"+Settings.com_ip+":8080/android/TerminateSA/"+Integer.toString(position));
            }

            urlConnection = (HttpURLConnection) urlToRequest.openConnection();
            //urlConnection.setConnectTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setReadTimeout(CONNECTION_TIMEOUT_TIME);
            urlConnection.setRequestMethod("GET");
            urlConnection.setUseCaches(false);
            urlConnection.setRequestProperty("connection", "close");
            urlConnection.connect();

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..
            }

        }catch(IOException e){
            e.printStackTrace();
            exception = 1;
        }finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        progressDialog.dismiss();
        serverCallBack.done();
        super.onPostExecute(aVoid);
        if (exception == 1){
            errorMessageDialog();
            Jobs job = new Jobs();
            job.setParameters("stopSA");
            job.setPosition(Integer.toString(position));
            db.addJob(job);
            //index.add(1); //INDEX IS USED TO LIST DONE OR UNDONE JOBS
        }
    }

    private static String getResponseText(InputStream inStream) {
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(inStream));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    /**
     *
     */
    private void errorMessageDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setMessage("Error occured while trying to connect!\n");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }
}
