package com.trito_paradoteo.project.myapplications.users;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public class User {

    private String name;
    private String service;
    private String username;
    private String password;

    public String getUsername() { return username; }

    public String getPassword() {
        return password;
    }

    public String getService() {
        return service;
    }

    public String getName() {
        return name;
    }

    public User(String name, String service, String username, String password){
        this.name = name;
        this.service = service;
        this.username = username;
        this.password = password;

    }

    public User(String username, String password){
        this.username = username;
        this.password = password;
        this.service = "";
        this.name = "";
    }
}
