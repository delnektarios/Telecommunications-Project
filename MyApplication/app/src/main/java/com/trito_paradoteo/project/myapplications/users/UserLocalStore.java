package com.trito_paradoteo.project.myapplications.users;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Nektarios Deligiannakis 1115201200030
 * Fwkiwnas Zikidis 1115201100037
 */
public class UserLocalStore {

    public static final String SP_NAME = "userDetails";
    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context){
        userLocalDatabase = context.getSharedPreferences(SP_NAME,0);
    }

    public void storeUserData(User user){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putString("name",user.getName());
        spEditor.putString("service",user.getService());
        spEditor.putString("username",user.getUsername());
        spEditor.putString("password",user.getPassword());
        spEditor.commit();
    }

    /**
     * get a logged in user details
     * @return User user
     */
    public User getLoggedUser(){
        String name = userLocalDatabase.getString("name", "");
        String service = userLocalDatabase.getString("service","");
        String username = userLocalDatabase.getString("username","");
        String password = userLocalDatabase.getString("password","");

        User existingUser = new User(name,service,username,password);
        return existingUser;
    }

    /**
     * set  a user is logged in
     * @param loggedIn bollean
     */
    public void setUserLoggedIn(boolean loggedIn){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.putBoolean("loggedIn",loggedIn);
        spEditor.commit();

    }

    /**
     * see if user is logged in
     * @return bollean true if yes/false otherwise
     */
    public boolean getUserLoggedIn(){
        return userLocalDatabase.getBoolean("loggedIn",false) == true;

    }

    /**
     * clearing shared preferences
     */
    public void clearUsersData(){
        SharedPreferences.Editor spEditor = userLocalDatabase.edit();
        spEditor.clear();
        spEditor.commit();
    }

}
