package Software_Agent;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */

public class Atomic_Check {

	private AtomicInteger flag = new AtomicInteger();
	
	/**
	 * setting flag to 1 leads to normal execution
	 */
	public Atomic_Check(){
		flag.set(1);
	}
	
	/**
	 * shutting down threads by setting flag value to 0
	 */
	public void kill9(){
		flag.set(0);
	}
	
	/**
	 * getting state of signal handler
	 * @return boolean format of integer values 0 or 1
	 */
	public boolean get_flag(){
		return flag.get()==1;
	}
	
}
