package Software_Agent;

import java.io.*;
import java.util.*;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */


/**
 * This class is for File read and write objects .
 * @param x : Is a scanner object that is allocated so the FileRW object can read from the file.
 * @ 
 */
public class FileRW {

		
	/**
 	* The costructor allocates a scanner and binds it to the the pathname given by the user from the stdin.  
 	*/
	public FileRW(){
		this.x = new Scanner( System.in );
		System.out.println("Enter path name : \n");
		while(true){
			String pathname = x.nextLine();
			try{
					File fl = new File(pathname);
					this.x = new Scanner(fl);
					break;
			
			}
			catch(FileNotFoundException e) {
		        System.err.println("File not found. Please scan in new file:");
		    }
		}
		
	}
	
	/**
	* The costructor allocates a scanner and binds it to the the pathname given by the user from the parameter pathnname.
	* @param pathname : Insert path for the file to be read or write. 
	*/	
	public FileRW(String pathname){
		
		System.out.printf("Creating file from default path;\n");
		try{
			
			File fl = new File(pathname);
			//System.out.printf(fl.getAbsolutePath());
			this.x = new Scanner(fl);
			
		}
		catch(FileNotFoundException e){
			System.err.printf("File not found. Please scan in new file." + e);		
		}
	}
	/**
	* Gets the nextline from the file. If there is no line to get it returns null
	*/
	public String read_line(){
		if(this.x.hasNextLine()){
			return this.x.nextLine();
		}
		return null;
	}
	
	public int read_int(){
		return this.x.nextInt();
 	}
	/**
	* Writes some text in the file. 
	* @param text : Text to be writen in the file. 	
	*/	
	public void write_in_file(String text){
		printer.write(text);
		return;
	}
	/**
	* Unbind the object from the file. 
	*/
	public void close(){
		this.x.close();
	}
	/**
	* The fuction will return all text until it reach the patern .
	* @ param pattern : Pattern String to be inserted for the use of delimitation   
	*/
	public void useDelimiter(String pattern){
		this.x.useDelimiter(pattern);
	}
	/**
	* Returns the tokken of characters until whitespace reached or EOF
	*/	
	public String readNext(){
		return this.x.next();
	}
	/**
	* Returns true of false depending if there is any characters left to return.
	*/ 
	public boolean hasNext(){
		return this.x.hasNext();
	}
	
	private Scanner x;
	private PrintWriter printer;
	
	
	
		

}
