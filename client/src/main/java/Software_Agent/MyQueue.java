package Software_Agent;

import java.util.Iterator;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */

/**
 * Custom Queue Implementation with Iterator .
 * Also Implements Producer Consumer problem . 
 * @param N : Integer counts the number of elements in the queue.
 * @param first : Node type that points to the first element of the line. 
 * @param last : Node type that points to the last element of the queue.
 *
 */

public class MyQueue<E> implements Iterable<E> {
	
	private int N = 0;
	private Node first;
	private Node last;
	/**
	* Constructs an iterator 
	*
	*/
	public Iterator<E> iterator() {
		return new QueueIterator();
	}
	
	/** 
	* Inner private class node so the queue can be implemented
	*
	*/
	class Node{
		private E element;
		private Node next;
		private Node previous;
	}

	/**
	* Constructor of the Queue
	*/
	public MyQueue(){
		this.first=null;
		this.last=null;
		
	}

	/** 
	* Check if the queue is empty 
	*/
	public boolean isEmpty(){
		if(this.first==null){
			return true;
		}
		else
			return false;
	}
	
	/** 
	*Get size of queue.
	*
	*/	
	public int getSize(){
		return this.N;
	}

	/**
	* Insert element in queue lock object 
	*
	*/
	public void insertData(E elem){
		Node x = new Node();
		x.element = elem;
		
		synchronized(this) {
		if (isEmpty()) {
	            first = x;
	            last = x;
	        } else {
	            last.previous = x;
	            x.next=last;
	            last = x; 
	        }
	        N++;
	        
	        this.notifyAll();
		}
	}
	
	
	/**
	* Get element from queue. Lock object while doing it. 
	*
	*/
	public E getData() throws InterruptedException{	
		E elem = null;
		
		synchronized(this) {
			while (isEmpty()) {
				this.wait();
			}
			
			elem = first.element;
			first=first.next;
			this.N = this.N-1 ;
		}
		return elem;
	}
	
	// ------------------ iterator ---------------------
	private class QueueIterator implements Iterator<E>{
		
		private Node current = initIterator();
		
		private Node initIterator(){
			return first;
		}
		
		public boolean hasNext() {
			if(current!=null){
				return true;
			}
			else {
				return false;
			}
		}

		
		public E next() {
			
			if (current==null){
				return null;
			}
			E elem = current.element;
			current=current.previous;
			return elem;		
		}
		
		public void remove() {}
		
	}
}



