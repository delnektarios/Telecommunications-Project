package Software_Agent;

import java.io.*;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */

/**
 * class Nmap_Job holds the job for one time job threads
 */
public class Nmap_Job {
	
	private String id;
	private String parameters;
	private String periodic;
	private String time;
	
	/**
	 * storing job
	 * @param str : holds job to be done
	 * @param time 
	 * @param periodic 
	 * @param parameters 
	 */
	public Nmap_Job(String id, String parameters, String periodic, String time){
		this.id = id;
		this.parameters = parameters;
		this.periodic = periodic;
		this.time = time;
		System.out.println("job inserted to queue with :"+id+parameters+periodic+time);
	}

	public String getId() {
		return id;
	}

	public String getParameters() {
		return parameters;
	}

	public String getPeriodic() {
		return periodic;
	}

	public String getTime() {
		return time;
	}
	

}
