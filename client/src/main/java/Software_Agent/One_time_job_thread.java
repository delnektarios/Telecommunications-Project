package Software_Agent;

import java.io.*;
import java.util.*;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */


public class One_time_job_thread implements Runnable {
	
	private String id = null;
	private String parameters = null;
	private String periodic = null;
	private String time = null;
	private String nmap_job;
	private MyQueue<Nmap_Job> queue;
	private Sender_thread resultQueue;
	private Atomic_Check flag;
	
	/**
	 * constructor prepares one time job thread
	 * @param queue : shared queue for hosting one time jobs
	 * @param sender: thread that collects and sends results to output
	 * @param flag : information about thread shutdown
	 */
	public One_time_job_thread(MyQueue<Nmap_Job> queue, Sender_thread sender, Atomic_Check flag){
		this.queue=queue;
		this.resultQueue=sender;
		this.flag = flag;
	}
	
	/**
	 * execution of nmap
	 */
	public void run(){
		
		Nmap_Job job = null;
		boolean flager = flag.get_flag();
		while(flager){
		/**
		 * getting a job
		 */
			try {
				job = queue.getData();
			} catch (InterruptedException e1) {
				System.out.println("caught interruption\n");
				return;
			}
			
			this.id = job.getId();
			this.parameters = job.getParameters();
			this.periodic = job.getPeriodic();
			this.time = job.getTime();
			
			if(!(this.parameters.contains("-oX -"))){
				this.parameters = this.parameters+ " -oX -";
				//System.out.println(this.parameters);
			}
			
			List<String> command = new ArrayList<String>();
			command.add("nmap");
			String[] string = this.parameters.split(" ");
			for(int i=0;i<string.length;i++){
				command.add(string[i]);
			}
	        //command.add(this.periodic.trim());
	        
	        System.out.println("printing arguments before nmap execution");
	        for(int i=0;i<command.size();i++){
	        	System.out.println(command.get(i));
	        }
	        
	        /**
	         * calling process builder for execution of nmap
	         */
			ProcessBuilder probuilder = new ProcessBuilder( command );
			
			//You can set up your work directory
			probuilder.directory(new File("."));
	    
			Process process = null;
			
			try {
				process = probuilder.start();
			} catch (IOException e2) {
				e2.printStackTrace();
				return;
			}
			
			//Read out directory output
			InputStream streamIn = process.getInputStream();
			InputStreamReader streamInReader = new InputStreamReader(streamIn);
			BufferedReader br = new BufferedReader(streamInReader);
			String line;
			
			File path = new File("file"+this.id);
			OutputStream outStream = null;
			try {
				outStream = new FileOutputStream(path);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
				process.destroy();
				return;
			}
			
			String buffer = "";
			
			flager = flag.get_flag();
			if(!flager){
				System.out.println("Exiting One time job;");
				process.destroy();
				return;
			}
			
			try {
				while ((line = br.readLine()) != null) {
					outStream.write(line.getBytes());
					buffer = buffer + line;
				}
			} catch (IOException e3) {
				// TODO Auto-generated catch block
				e3.printStackTrace();
				process.destroy();
				return;
			}
	    
			//Wait to get exit value
			try {
				int exitValue = process.waitFor();
				System.out.println("\n\nExit Value is " + exitValue + "from thread " + this.id);
			} catch (InterruptedException e) {
				//e.printStackTrace();
				System.out.println("Interruption Occured.No need for return value;");
				process.destroy();
				return;
			}
			
			try {
				outStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			flager = flag.get_flag();
			if(!flager){
				System.out.println("Exiting One time job;");
				return;
			}
			
			resultQueue.add_to(buffer);
		}
		
		
	}


}
