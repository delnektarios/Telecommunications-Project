package Software_Agent;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */

public class Periodic_thread implements Runnable {
	
	private String id = null;
	private String parameters = null;
	private String periodic = null;
	private String time = null;
	private Sender_thread resultQueue;
	private Atomic_Check flag;
	
	/**
	 * constructor prepares arguments for executing nmap periodically
	 * @param id : holds nmap job's id
	 * @param time 
	 * @param periodic 
	 * @param parameters
	 * @param sender : object to send results to
	 * @param flag : holds information for shutting down thread
	 */
	public Periodic_thread(String id, String parameters, String periodic, String time, Sender_thread sender, Atomic_Check flag){
		this.id = id;
		this.parameters = parameters;
		this.periodic = periodic;
		this.time = time;
		this.resultQueue=sender;
		this.flag = flag;
	}
	
	/**
	 * method run executes nmap and sends results to sender  
	 */
	public void run(){
			
			int time = Integer.parseInt(this.time);
			
			if(!(this.parameters.contains("-oX -"))){
				this.parameters = this.parameters+ " -oX -";
				//System.out.println(this.parameters);
			}
			
			System.out.println("Running periodic thread\n");
			List<String> command = new ArrayList<String>();
			command.add("nmap");
			String[] string = this.parameters.split(" ");
			for(int i=0;i<string.length;i++){
				command.add(string[i]);
			}
			//command.add(this.periodic.trim());
			//command.add(this.time);
	        System.out.println("printing arguments before nmap execution in  periodic");
	        for(int i=0;i<command.size();i++){
	        	System.out.println(command.get(i)+"\n");
	        }
	        
	        /**
	         * calling Processbuilder for external execution of nmap
	         */
			ProcessBuilder probuilder = new ProcessBuilder( command );
			
			//You can set up your work directory
			probuilder.directory(new File("."));
	    
			Process process = null;
			
			boolean flager = flag.get_flag(); //informing thread whether or not its time to shutdown
			
			while(flager){
			
				flager = flag.get_flag();//informing thread whether or not its time to shutdown
				if(!flager){
					System.out.println("Exiting periodic job;");
					break;
				}
				
				try {
					process = probuilder.start();
				} catch (IOException e2) {
					e2.printStackTrace();
					return;
				}
			
				//Read out directory output
				InputStream streamIn = process.getInputStream();
				InputStreamReader streamInReader = new InputStreamReader(streamIn);
				BufferedReader br = new BufferedReader(streamInReader);
				String line;
			
				File path = new File("file"+this.id);
				OutputStream outStream = null;
				try {
					outStream = new FileOutputStream(path);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
					process.destroy();
					return;
				}
			
				String buffer = "";
				
				flager = flag.get_flag();//informing thread whether or not its time to shutdown
				if(!flager){
					System.out.println("Exiting periodic job;");
					process.destroy();
					break;
				}
			
				try {
					while ((line = br.readLine()) != null) {
						outStream.write(line.getBytes());
						buffer = buffer + line;
					}
				} catch (IOException e3) {
					// TODO Auto-generated catch block
					e3.printStackTrace();
					process.destroy();
					return;
				}
	    
				//Wait to get exit value
				try {
					int exitValue = process.waitFor();
					//System.out.println("\n\nExit Value is " + exitValue);
					System.out.println("\n\nExit Value is " + exitValue + "from thread" + this.id);
				} catch (InterruptedException e) {
					//e.printStackTrace();
					System.out.println("(periodic : )Interruption Occured.No need for return value;");
					process.destroy();
					return;
				}
			
				try {
					outStream.close();
				} catch (IOException e) {
					e.printStackTrace();
					return;
				}
				
				flager = flag.get_flag();//informing thread whether or not its time to shutdown
				if(!flager){
					System.out.println("Exiting periodic job;");
					break;
				}
				
				resultQueue.add_to(buffer);
				
				try {
					Thread.sleep(time*60*1000);
				} catch (InterruptedException e) {
					System.out.println("Interruption;");
					return;
				}
			}
	
		
	}


}
