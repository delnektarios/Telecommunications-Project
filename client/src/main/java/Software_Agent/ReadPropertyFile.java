package Software_Agent;

import java.io.*;
import java.util.Properties;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */

/**
 * 
 * Class ReadPropertyFile reads the handles the .congig files
 *
 */
public class ReadPropertyFile {
	
	private Properties prop = null;
	private FileReader reader ;
	
	/**
	 * constructor opens necessary config file 
	 * @param path : name of file
	 */
	public ReadPropertyFile(String path){
		File fl = new File(path);
		this.prop = new Properties();
		
		try {
			this.reader = new FileReader(fl) ;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		try {
			prop.load(reader);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * 
	 * @return : number of threads for thread pool
	 */
	public int getNumThread(){
		String str = prop.getProperty("Thread_Num");
		return Integer.parseInt(str);
	}
	
	/**
	 * 
	 * @return value for main to sleep
	 */
	public int getSleepTime(){
		String str = prop.getProperty("Sleep_Time");
		return Integer.parseInt(str);
	}

}
