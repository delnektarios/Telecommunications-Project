package Software_Agent;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.json.simple.JSONObject;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */

public class Sender_thread {
	/**
	 * resultQueue keeps trace of all results and sends them periodically to output
	 */
	private Client client;
	private BlockingQueue<String> resultQueue = new LinkedBlockingQueue<String>();
	private boolean run;
	
	/**
	 * constructor
	 */
	public Sender_thread(){
		System.out.println("sender created");
		this.run = true;
	}
	
	/**
	 * method to terminate sender
	 */
	public void stop_sender(){
		this.run = false;
	}
	
	/**
	 * method to handle shared data structure for the results
	 * @param to_send : string to be inserted to shared queue and to be sent
	 */
	public void add_to(String to_send){
		System.out.println("insert string");
		resultQueue.add(to_send);
		System.out.println("size of sender "+to_send);
	}
	
	/**
	 * method run periodically collects and sends results to output
	 * @param hash_key 
	 * @throws InterruptedException 
	 */
	public void to_run(Client client, int hash_key) {
		
		this.client = client;
		String buffer = null;
		
		while(run){
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			try {
				buffer = (String) this.resultQueue.take();
			} catch (InterruptedException e1) {
				//e1.printStackTrace();
				return;
			}

			//----------------------sending results to server--------------
    		
			JSONObject input=new JSONObject();
			
			try {
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory
						.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				//org.w3c.dom.Document doc = dBuilder.parse(buffer);
				
				//http://stackoverflow.com/questions/1706493/java-net-malformedurlexception-no-protocol
				org.w3c.dom.Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(buffer.getBytes("utf-8"))));
				
				// http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
				((org.w3c.dom.Document) doc).getDocumentElement().normalize();

				System.out.println("Root element :"
						+ ((org.w3c.dom.Document) doc).getDocumentElement().getNodeName());

				NodeList nList = ((org.w3c.dom.Document) doc).getElementsByTagName("nmaprun");
				
				for (int temp = 0; temp < nList.getLength(); temp++) {
					
					Node nNode = nList.item(temp);
					System.out.println("\nCurrent Element :" + nNode.getNodeName());

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {

						Element eElement = (Element) nNode;
						
						System.out.println("Scanner : "
								+ eElement.getAttribute("scanner"));
						input.put("scanner", eElement.getAttribute("scanner").toString());
						System.out.println("Args : "
								+ eElement.getAttribute("args"));
						input.put("cmd_arguments", eElement.getAttribute("args").toString());
						System.out.println("Start : "
								+ eElement.getAttribute("start"));
						input.put("start", eElement.getAttribute("start").toString());
						System.out.println("Startstr : "
								+ eElement.getAttribute("startstr"));
						input.put("startstr", eElement.getAttribute("startstr").toString());
						System.out.println("Version : "
								+ eElement.getAttribute("version"));
						input.put("version", eElement.getAttribute("version").toString());
						
					}
				}
				/*
				nList = ((org.w3c.dom.Document) doc).getElementsByTagName("port");
				Node nNode = nList.item(0);
				if (nNode.getNodeType() == Node.ELEMENT_NODE){
					Element eElement = (Element) nNode;
					System.out.println("Protocol : "
							+ eElement.getAttribute("protocol"));
					input.put("protocol", eElement.getAttribute("protocol").toString());
				}
				nList = ((org.w3c.dom.Document) doc).getElementsByTagName("finished");
				nNode = nList.item(0);
				if (nNode.getNodeType() == Node.ELEMENT_NODE){
					Element eElement = (Element) nNode;
					System.out.println("Summary : "
							+ eElement.getAttribute("summary"));
					input.put("summary", eElement.getAttribute("summary").toString());
				}	
				*/			
			} catch (Exception e) {
				e.printStackTrace();
			}
			input.put("Hash_key", Integer.toString(hash_key));
			System.out.println(input.get("scanner")+" "+input.get("cmd_arguments")+" "+input.get("start")+" "+input.get("startstr")+" "+input.get("version"));
			input.put("Hash_key", Integer.toString(hash_key));
			WebResource webResource = client.resource("http://localhost:8080/Jobs/results");
			ClientResponse response = webResource
					.type("application/json")
					.put(ClientResponse.class, input.toJSONString());
			if (response.getStatus() != 204) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			
			//System.out.println(buffer);
		}
		
	}

}
