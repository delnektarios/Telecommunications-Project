
package Software_Agent;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Random;

import javax.swing.JOptionPane;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * 
 * @author Deligiannakis Nektarios 1115201200030
 * @author Zikidis Fwkiwnas 1115201100037
 *
 */
public class Software_Agent {
	
	
	
	public static void main(String[] args) throws InterruptedException, ParseException, UnknownHostException, SocketException{
		
	    Random rand = new Random(System.currentTimeMillis());
	    
	    /**
	     * share queue for one time jobs
	     */
	    MyQueue<Nmap_Job> queue = new MyQueue<Nmap_Job>();
	    //ConcurrentLinkedQueue<String> resultQueue = new ConcurrentLinkedQueue<String>();
	    
	    /**
	     * list to keep track of periodic threads
	     */
	    final List<Thread> periodic_threads = new ArrayList<Thread>();
	    
	    List<Integer> periodic_id = new ArrayList<Integer>();
	    
	    /**
	     * object used for shutdown after receiving Ctrl-C
	     */
	    final Atomic_Check flag = new Atomic_Check();
	    
	    /**
	     * property file
	     */
		final ReadPropertyFile pr = new ReadPropertyFile("config.properties");
		
		/**
		 * sender thread
		 */
		final Sender_thread sender = new Sender_thread();
		
		
		//-----------------------------------------------------------------------------------------
		//http://www.mkyong.com/java/how-to-get-mac-address-in-java/ CODE USED FOR IP AND MAC ADDRESS 
		JSONObject input=new JSONObject();
		input.put("Device_Name","lenovo");
		
		InetAddress IP=InetAddress.getLocalHost();
		System.out.println("IP of my system is := "+IP.getHostAddress());//it is the same to IP.toString | getHostAddress returns String
		input.put("Interface_IP", IP.toString());
		
		byte[] mac = null;
		//http://stackoverflow.com/questions/6595479/java-getting-mac-address-of-linux-system
	    Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
	        while(networkInterfaces.hasMoreElements())
	        {
	            NetworkInterface network = networkInterfaces.nextElement();
	            System.out.println("network : " + network);
	            mac = network.getHardwareAddress();
	            if(mac == null)
	            {
	                System.out.println("null mac");             
	            }
	            else
	            {
	                System.out.print("MAC address : ");

	                StringBuilder sb = new StringBuilder();
	                for (int i = 0; i < mac.length; i++)
	                {
	                    sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));        
	                }
	                System.out.println(sb.toString());  
	                break;
	            }
	        }
		
		NetworkInterface network = NetworkInterface.getByInetAddress(IP);
		System.out.println(network);
		
		
		System.out.print("Current MAC address : ");		
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length; i++) {
			sb.append(String.format("%02X%s", mac[i],(i < mac.length - 1) ? "-" : ""));		
		}
		System.out.println(sb.toString());
		input.put("Interface_MacAddr", sb.toString());
		
		//http://www.javacoderanch.com/how-to-get-operating-system-name-and-version.html
        // The key for getting operating system name
        String name = "os.name";        
        // The key for getting operating system version
        String version = "os.version";        
        // The key for getting operating system architecture
        String architecture = "os.arch";
        
        System.out.println("Name: " + System.getProperty(name));
        System.out.println("Version: " + System.getProperty(version));
        System.out.println("Architecture: " + System.getProperty(architecture));
		input.put("Os_Version",System.getProperty(name)+" "+System.getProperty(version)+" "+System.getProperty(architecture) );
		input.put("NMap_Version", "6.40");
		//generating hash key
		final int hash_key = input.hashCode();
		input.put("Hash_key", Integer.toString(hash_key));
		//System.out.println("hash key from client "+input.get("Hash_key"));
		
		final Client client = Client.create();
		WebResource webResource = client.resource("http://localhost:8080/register/question");
		System.out.println("response");
		System.out.println(input);
		ClientResponse response = webResource
		    .type("application/json")
		    .put(ClientResponse.class, input.toJSONString());
		if (response.getStatus() != 204) {
 		   throw new RuntimeException("Failed : HTTP error code : "
 			+ response.getStatus());
 		}
		System.out.println("after response");
		//Thread.sleep(5000);
		
		webResource = client.resource("http://localhost:8080/register/answer");
		response = webResource.accept("application/json")
				.get(ClientResponse.class);    
		if (response.getStatus() != 200) {
		   throw new RuntimeException("Failed : HTTP error code : "
			+ response.getStatus());
		}
		
		String output = response.getEntity(String.class);
		System.out.println("Output from Server .... \n");
		System.out.println(output);
		
		JSONObject obj = (JSONObject) new JSONParser().parse(output);
		String answer = (String) obj.get("answer");
		
		if(answer.equals("permision granted")){
			System.out.println("Successfuly Established Connection");
		}
		else{
			JOptionPane.showMessageDialog(null, "AM Denied Service.\nThrowing Runntime Exception!");
 		   	throw new RuntimeException("Failed to acquire permission.Exiting");
		}
		
		
		/**
		 * starting sender thread
		 */
		final Thread sender_thread = new Thread(new Runnable(){
			public void run(){
				sender.to_run(client,hash_key);
			}
		});
		sender_thread.start();
		System.out.println("sender thread generated!");
		/**
		 * creation of thread pool
		 */
		final Thread[] threads =new Thread[pr.getNumThread()];
		for (int i=0;i<pr.getNumThread();i++) {
			One_time_job_thread runnable =new One_time_job_thread(queue,sender,flag); 
			threads[i] = new Thread(runnable);
			threads[i].start();
		}

		System.out.println("all one time thread generated!");
        
		//--------------------------functionality of SA-----------------------------------
		while(true){
			
			/*
			input=new JSONObject();
			input.put("SA_ID", Integer.toString(hash_key));
			webResource = client.resource("http://localhost:8080/Jobs/requestJob");
			response = webResource
					.type("application/json")
					.put(ClientResponse.class, input.toJSONString());
			if (response.getStatus() != 204) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			
			Thread.sleep(5000);
			
			System.out.println("going to sleep");
			Thread.sleep(5000);
			System.out.println("awaking from a 5 second sleep");
			*/
			webResource = client.resource("http://localhost:8080/Jobs/getJob/"+Integer.toString(hash_key));
			response = webResource.accept("application/json")
				.get(ClientResponse.class);    
			if (response.getStatus() != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ response.getStatus());
			}
			
			output = response.getEntity(String.class);
			System.out.println("got jobs from server "+output+"\n");
			
			//IF THERE ARE NOT ANY JOBS PROGRAMM WILL BEHAVE UNEXPECTED
			
			List<JSONObject> jobs_list = (List<JSONObject>) new JSONParser().parse(output);
			output = null;	
			
			for(int i=0;i<jobs_list.size();i++){
				JSONObject JSONjob = jobs_list.get(i);
				String id = (String) JSONjob.get("id");
				if(id.equals("-0")){
					System.out.println("No jobs for me right now!");
					continue;
				}
				String parameters = (String) JSONjob.get("parameters");
				String periodic = (String) JSONjob.get("periodic");
				System.out.println(periodic);
				String time = (String) JSONjob.get("time");
				System.out.println(id+" "+parameters+" "+periodic+" "+time+"\n");
				//create Nmap_Job object and insert to list FIRST PROJECT
				if(periodic.equals("false")){
					Nmap_Job job = new Nmap_Job(id,parameters,periodic,time);
					queue.insertData(job);
				}
				else if(periodic.equals("true")){
					System.out.println("INSIDE THE PERIODIC CODE!");
					//String SA_termination = "-1, exit(0), true, -1";
					System.out.println(id);
					System.out.println(id.equals("-1"));
					System.out.println(parameters);
					System.out.println(parameters.equals("exit(0)"));
					System.out.println(time);
					System.out.println(time.equals("-1"));
					//if(id.equals("-1") && parameters.equals("exit(0)") && time.equals("-1")){
					if(parameters.equals("exit(0)")){
						System.out.println("Processing for shutdown!");				                
				       	flag.kill9();				                
				      	for (int k=0;k<pr.getNumThread();k++) {
				        	threads[k].interrupt();
				        	try {
								threads[k].join();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
				      	}				                				                
				    	System.out.println("exited one time threads\n");
				       	for(int j=0;j<periodic_threads.size();j++){
				       		periodic_threads.get(j).interrupt();
				       		try {
				       			periodic_threads.get(j).join();
				       		} catch (InterruptedException e) {
				       			e.printStackTrace();
				       		}
				     	}
				       	sender.stop_sender();
				       	sender_thread.interrupt();
				      	try {
				        	sender_thread.join();
				      	} catch (InterruptedException e) {
				        	e.printStackTrace();
				     	}
				       	System.out.println("ready to shutdown;");
				       	//MISSING COMMAND TO TERMINATE PROGRAMM HERE
				       	System.exit(0);							
					}
					//message to send to client "periodic_id, Stop, true, periodic"
					else if(parameters.equals("Stop") && time.equals("periodic")){
						//TERMINATING periodic job
						for(int j=0;i<periodic_id.size();j++){
							String id_rm = Integer.toString(periodic_id.get(j));
							if(id.equals(id_rm)){
								periodic_threads.get(j).interrupt();
								periodic_threads.remove(j);
								periodic_id.remove(j);
								break;
							}
						}
						continue;
					}
					System.out.println("PERIODIC THREAD!");
					System.out.println("executing the periodic job received from service!");
					periodic_id.add(Integer.parseInt(id));
					Thread t = new Thread(new Periodic_thread(id,parameters,periodic,time,sender,flag));
					periodic_threads.add(t);
					t.start();
				}
			}

			
			
			try {
				Thread.sleep(pr.getSleepTime());
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			//CHECKING THE THREADS IF THEY ARE ALIVE OR NOT
			for (int i=0;i<pr.getNumThread();i++) {
				boolean alive = threads[i].isAlive();
				if(alive == true) continue;
				System.out.println("threaD IS NOT ALIVE. regen.");
				One_time_job_thread runnable =new One_time_job_thread(queue,sender,flag); 
				threads[i] = new Thread(runnable);
				threads[i].start();
			}
		}
	}
}
